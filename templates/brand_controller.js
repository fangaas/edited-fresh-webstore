app.controller('brand_controller', ['$scope', '$routeParams', 'moltinApiService', '$location', function($scope, $routeParams, moltinApiService, $location) {

	$(function() {

		$("body").animate({scrollTop:0});
		$('.sidebar-menu ul').slideUp(300);

	});

	$scope.selectBrand = function(brandSlug) {
		$location.path("/filter").search('type', 'brand').search('val', brandSlug);
	}

	$scope.displayBrands = [{
		name: "Boskke",
		image: "boskke.jpg"
	}, {
		name: "ByALEX",
		image: "byalex.jpg"
	}, {
		name: "Ferm-Living",
		image: "fermliving.jpg"
	}, {
		name: "HAY",
		image: "hay.jpg"
	}, {
		name: "Mini-Moderns",
		image: "minimoderns.jpg"
	}, {
		name: "Newgate-Clocks",
		image: "newgateclocks.jpg"
	}, {
		name: "Normann-Copenhagen",
		image: "normanncopenhagen.jpg"
	}, {
		name: "NUD-Collection",
		image: "nudcollection.jpg"
	}, {
		name: "Plumen",
		image: "plumen.jpg"
	}, {
		name: "Seletti",
		image: "seletti.jpg"
	}, {
		name: "Seven-Gauge-Studios",
		image: "sevengaugestudios.jpg"
	}, {
		name: "Woouf",
		image: "woouf.jpg"
	}, {
		name: "Wrong-for-HAY",
		image: "wrongforhay.jpg"
	}];

	$scope.runImageLoader();

}]);
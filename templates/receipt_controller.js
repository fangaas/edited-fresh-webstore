app.controller('receipt_controller', ['$scope', '$routeParams', 'moltinApiService', 'remoteRequestService' , function($scope, $routeParams, moltinApiService, remoteRequestService) {

	console.log("Receipt controller");

	var orderId = $routeParams.orderId;

	//Retrieve order information and items to display receipt thing	

	$scope.receiptSummaryJson = null;

	$scope.receiptItemJson = null;

	function getDisplayData() {

		remoteRequestService.getOrderSummary(orderId, getSummaryJsonCallback, getJsonErrorCallback);

	}

	function getSummaryJsonCallback(data) {

		$scope.orderSummary = data.data.result;

		remoteRequestService.getOrderItems(orderId, getItemJsonCallback, getJsonErrorCallback);

	}

	function getItemJsonCallback(data) {

		$scope.receiptItemJsonArray = data.data.result;

	}

	function getJsonErrorCallback() {

		console.log("Error getting data for order");

	}

	getDisplayData();

	$("body").animate({scrollTop:0});



}]);

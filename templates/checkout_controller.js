app.controller('checkout_controller', ['$scope', 'moltinApiService', 'remoteRequestService', '$location', '$timeout', '$routeParams', 'eventTracking', function($scope, moltinApiService, remoteRequestService, $location, $timeout, $routeParams, eventTracking) {

	$scope.setupPaypalButton = function() {

		paypal.checkout.setup('7F5S69FGG6U3N', {
			//environment: 'sandbox',
			environment: 'production',
			container: 'paypalInContext',
			click: function() {

				$scope.redirectingToPaypal = true;

				paypal.checkout.initXO();

				$scope.convertCartToOrder($scope.getOrderJson(), getPaypalTokenCallback);

				function getPaypalTokenCallback(order) {

					$scope.moltinOrder = order;

					var accountCreationData = {
				    	createAccount: $scope.createAccount,
				    	useCustomPassword: $scope.createAccountCustomPassword,
				    	customPassword: $scope.customPassword,
				    	signupNewsletter: $scope.signupNewsletter
				    };

				    if($scope.loggedIn) accountCreationData.createAccount = false;

					var total = $scope.getFinalTotalWithDeliveryNoPretty().toFixed(2);
					remoteRequestService.paypalSetExpressCheckout(total, order.id, accountCreationData, paypalSuccessCallback, failure);
				}

				function paypalSuccessCallback(data) {
					var url = paypal.checkout.urlPrefix + data.data.token;
					paypal.checkout.startFlow(url);
				}

				function failure() {

				}


			}
		});
		
	}


	$(function() {

		var getAddressApiKey = "SpJB2PViKEuI0L6FbIdkEw6479";

		//Page ready functions
		$(".lookupShippingAddress").getAddress({
			api_key: getAddressApiKey,
			output_fields: {
				line_1: '#shipping_address',
		        line_2: '#shipping_address_second',
		        post_town: '#shipping_city',
		        county: "#shipping_state",
		        postcode: '#shipping_postcode'
			},
			onAddressSelected: function(elem, index) {
				$scope.shippingShowAddressFields = true;
				$scope.$apply();

				//Hack needed here to work with Angular
				var address = elem.split(",");

				//0 - address 1
				//5 - city
				//6 - county
				$('#shipping_country option[value="GB"]').prop('selected', true).trigger("change");
				$("#shipping_first_name").trigger("change");
				$("#shipping_last_name").trigger("change");
				$("#shipping_city").trigger("change");
				$("#shipping_postcode").trigger("change");
				$("#shipping_address").trigger("change");
				$("#shipping_state").trigger("change");
				//$('#shipping_state option[value="' + address[6].trim() + '"]').prop('selected', true).trigger("change");

			}
		});

		$(".lookupBillingAddress").getAddress({
			api_key: getAddressApiKey,
			output_fields: {
				line_1: '#billing_address',
		        line_2: '#billing_address_second',
		        post_town: '#billing_city',
		        county: "#billing_state",
		        postcode: '#billing_postcode'
			},
			onAddressSelected: function(elem, index) {
				$scope.billingShowAddressFields = true;
				$scope.$apply();

				//Hack needed here to work with Angular
				var address = elem.split(",");

				//0 - address 1
				//5 - city
				//6 - county
				$('#billing_country option[value="GB"]').prop('selected', true).trigger("change");
				$("#billing_first_name").trigger("change");
				$("#billing_last_name").trigger("change");
				$("#billing_city").trigger("change");
				$("#billing_postcode").trigger("change");
				$("#billing_address").trigger("change");
				$("#billing_state").trigger("change");
				//$('#billing_state option[value="' + address[6].trim() + '"]').prop('selected', true).trigger("change");

			}
		});

		/*var selectizeStateConfig = {
		    create: true,
		    sortField: 'text'
		}

		$("#shipping_state").selectize(selectizeStateConfig);
		$("#billing_state").selectize(selectizeStateConfig);*/

		//window.setTimeout($scope.loadExistingCustomData, 500);

		/*card = new Skeuocard($("#skeuocard"), {
			debug: true,
			dontFocus: true
		});*/

		//Scroll back to the top
		$("body").animate({scrollTop:0});
		$('.sidebar-menu ul').slideUp(300);

	});

	$scope.countryList = [{
		code: "GB",
		name: "United Kingdom (Great Britain)"	
	}, {
	    name: "Ireland",
	    code: "IE",
	    cost: 12.95
	}, {
		code: "FR",
		name: "France",
		cost: 13.95	
	}, {
	    name: "Monaco",
	    code: "MC",
	    cost: 13.95
	}, {
		code: "DK",
		name: "Denmark",
	    cost: 16.95
	}, {
	    name: "Belgium",
	    code: "BE",
	    cost: 9.95
	}, {
	    name: "Luxembourg",
	    code: "LU",
	    cost: 9.95
	}, {
		code: "DE",
		name: "Germany",
	    cost: 10.95
	}, {
		code: "ES",
		name: "Spain",
	    cost: 25.95	
	}];

	$scope.shippingAvailableCounties = [];

	$scope.sectionHeights = {
		"contact_data_form": "0px",
		"shipping_address_form": "0px",
		"billing_address_form": "0px",
		"delivery_options": "0px",
		"cart_items": "0px",
		"payment": "0px",
		"confirm": "0px"
	};

	$scope.sectionOrder = ["contact_data_form", "shipping_address_form", "billing_address_form", "delivery_options", "cart_items", "payment", "confirm"];

	$scope.sectionOrderWithNoBilling = ["contact_data_form", "shipping_address_form", "delivery_options", "cart_items", "payment", "confirm"];

	var currentSection = "contact_data_form";

	$scope.trackedSections = [];

	$scope.completedSections = [];

	//Which section order array index are we looking at here
	$scope.currentSectionNumber = 0;

	//If emails dont match in contact form
	$scope.contact_emailsDontMatch = false;

	//If user has selected that the billing address is the same as the shipping address
	$scope.billingSameAsShipping = false;

	//if you should display the saved address finder
	$scope.shippingShowSavedAddresses = true;
	$scope.billingShowSavedAddresses = true;

	//If you should display or show the address finder 
	$scope.shippingShowAddressFinder = false;
	$scope.billingShowAddressFinder = false;

	//If user has clicked to enter manual address or has selected address from list then display address entry fields
	$scope.shippingShowAddressFields = false;
	$scope.billingShowAddressFields = false;

	//Vars for managing delivery
	$scope.deliverySpanSize = "span6";
	$scope.availableDeliveries;
	$scope.chosenDelivery = null;
	$scope.deliveryPrice;
	$scope.internationalShippingCost = 12.95;
	$scope.finalTotal;

	//Vars for managing payments
	$scope.paymentSpanSize = "span6";
	$scope.availablePayments = ["stripe", "paypal"];
	$scope.chosenPayment;
	$scope.paymentChosen = false;
	$scope.paymentReady = false;

	//Vars for managing user
	//If the users email has been checked for existing account
	$scope.userEmailChecked = false;
	$scope.userEmailCheckInProgress = false;
	//If the users email has been recognised and may want to login
	$scope.userRecognised = false;

	//Create account at same time
	$scope.createAccount = false;
	//Signup to newsletter at same time (mailchimp api)
	$scope.signupNewsletter = $scope.loggedIn ? false : false;
	//Create account with custom password
	$scope.createAccountCustomPassword = false;
	$scope.customPassword;
	$scope.customPasswordRepeat;
	$scope.customPasswordsDontMatch = false;

	$scope.skeuocardInitialised = false;
	$scope.card;
	$scope.cardValid = false;

	//Wait until order appears
	$scope.waitOrderTimeout = 5000;
	$scope.longWaitOrderTimeout = 8000;


	//If all data necessary for checkout has been retrieved
	$scope.allDataObtained = false;

	$scope.confirmationCart = false;

	//Getting order details etc.
	$scope.preparingOrder = false;

	$scope.paymentInProgress = false;
	$scope.paymentError = false;
	$scope.paymentErrorInfo = "";

	$scope.stripeAuthError = false;
	$scope.stripeErrorInfo = "";

	//Object to hold the vex payment overlay
	$scope.paymentOverlay;

	//Post authorisation variables
	$scope.storedOrderId;
	//Moltin order for display after return url etc.
	$scope.moltinOrder;

	$scope.redirectedToReceipt = false;

	$scope.conversionTracked = false;


	//paypal payment process
	/*$scope.redirectingToPaypal = false;
	$scope.paypalRedirected = false;
	$scope.paypalCancelled = false;
	$scope.paypalRedirectSuccess = false;
	$scope.paypalToken;
	$scope.paypalPayerId;*/

	$scope.deliveryCosts = {
		"free": 0,
		"collect": 0,
		"next-day": 4.95,
		"shipping": $scope.internationalShippingCost
	};

	function setAngularInput(selector, value) {
		$(selector).val(value);
		$(selector).trigger("input");
	}


	$scope.bindWindowLeave = function() {
		//cross browser compatible event adding
		window.loggedIn = $scope.loggedIn;
		window.retainerPopupShown = false;
		window.mouseHasLeft = false;

		function addEvent(obj, evt, fn) {
		    if (obj.addEventListener) {
		        obj.addEventListener(evt, fn, false);
		    }
		    else if (obj.attachEvent) {
		        obj.attachEvent("on" + evt, fn);
		    }
		}

		function showFeedbackPopup() {

			if(!window.mouseHasLeft | window.retainerPopupShown) return; 

			window.retainerPopupShown = true;
        	vex.dialog.buttons.YES.text = 'Send Feedback';
			vex.dialog.buttons.NO.text = 'Close';

			eventTracking.showFeedbackPopup();

			vex.dialog.open({
				message: '<h1>Just wondering...</h1><div class = "vex-text">Is there anything we could do to improve the online experience you are having with us today?</div><div class = "vex-text vex-text-small">All feedback is anonymous and greatly appreciated!</div>',
				input: '<textarea class = "vex-feedback-input" name = "feedback" placeholder = "Enter your feedback here"></textarea>',
				defaultOptions: {
					showCloseButton: false,
					escapeButtonCloses: true,
					overlayClosesOnClick: true,
					appendLocation: 'body',
					className: 'vex-body',
					overlayClassName: 'vex-overlay',
					contentClassName: 'vex-content',
					closeClassName: 'vex-close'
				},
				callback: function(data) {
					console.log(data);
					if(data && data.feedback !== "") {
						vex.dialog.buttons.YES.text = "You're welcome";
						vex.dialog.alert('<h1>Thanks for your feedback!</h1>');

						var bodyObject = {
			    			message: data.feedback
			    		}

			    		$.post("http://niftyretail.com:4830/feedbackEmail", bodyObject, function(response) {});
					}
				}

			})
		}

		//POPUP DISABLED FOR NOW
		addEvent(document, "mouseout", function(e) {
		    e = e ? e : window.event;
		    var from = e.relatedTarget || e.toElement;
		    if (!from || from.nodeName == "HTML") {
		        // stop your drag event here
		        // for now we can just use an alert
		        if(!window.loggedIn && !window.retainerPopupShown) {
		        	
		        	window.mouseHasLeft = true;

		        	window.setTimeout(showFeedbackPopup, 2000);

		        }
		    }
		});

		addEvent(document, "mouseenter", function(e) {
			window.mouseHasLeft = false;
		});
	}

	//$scope.bindWindowLeave();

	$scope.loadExistingCustomData = function() {

		if($scope.loggedIn) {
			//If user is logged in then go ahead and load in this data
			//Load addresses
			function customerAddressCallback() {
				if($scope.loggedInCustomerAddresses.length == 0) {

					$scope.shippingShowSavedAddresses = false;
					$scope.billingShowSavedAddresses = false;

					$scope.shippingShowAddressFinder = true;
					$scope.billingShowAddressFinder = true;

				}
			}

			$scope.loadCustomerAddresses(customerAddressCallback);
			
			setAngularInput("#contact_firstName", $scope.loggedInCustomerJson.first_name);
			setAngularInput("#contact_lastName", $scope.loggedInCustomerJson.last_name);
			setAngularInput("#contact_email", $scope.loggedInCustomerJson.email);
			setAngularInput("#contact_email_repeat", $scope.loggedInCustomerJson.email);

			setAngularInput("#shipping_first_name", $scope.loggedInCustomerJson.first_name);
			setAngularInput("#shipping_last_name", $scope.loggedInCustomerJson.last_name);
			setAngularInput("#billing_first_name", $scope.loggedInCustomerJson.first_name);
			setAngularInput("#billing_last_name", $scope.loggedInCustomerJson.last_name);


			//Skip first section as detials already in place 
			if(!$routeParams.paypal) {
				$scope.sectionHeights["shipping_address_form"] = "1500px";
				$scope.currentSectionNumber = 1;
				currentSection = "shipping_address_form";
				$scope.completedSections.push("contact_data_form");
			}

		} else {

			//Not logged in so go from start and show all address finders etc.
			$scope.sectionHeights["contact_data_form"] = "1500px";
			$scope.shippingShowAddressFinder = true;
			$scope.billingShowAddressFinder = true;

		}

	}

	$timeout($scope.loadExistingCustomData, 100);
	//loadExistingCustomData();

	$scope.getMoltinCheckoutData = function() {

		function shippingMethodCallback(methods) {
			$scope.allShippingMethods = methods;
			$scope.allDataObtained = true;
		}

		moltinApiService.getAllShippingMethods(shippingMethodCallback);

	}

	//Get the data when a user fires up the page
	$scope.getMoltinCheckoutData();

	function manualValidation() {
		var extraValid = true;

		//Validation angular just can't quite handle
		if($scope.order.email !== $scope.order.email_repeat) {
			$scope.contact_emailsDontMatch = true;
			extraValid = false;
		} else {
			$scope.contact_emailsDontMatch = false;
		}

		if($scope.createAccountCustomPassword) {
			if($scope.customPassword !== $scope.customPasswordRepeat) {
				$scope.customPasswordsDontMatch = true;
				extraValid = false;
			} else {
				$scope.customPasswordsDontMatch = false;
			}
		}

		return extraValid;
	}

	$scope.goNextSection = function() {

		//goto the next section
		var sectionsWithValidation = ["contact_data_form", "shipping_address_form", "billing_address_form"];

		var noValidation = (sectionsWithValidation.indexOf(currentSection) == -1);
		
		//Set current section validation
		if(!noValidation) this[currentSection].$setSubmitted();

		//if(this[currentSection].$valid && manualValidation(this.order)) {
		if(noValidation || (this[currentSection].$valid && manualValidation())) {

			//Trigger autocomplete remember hack
			//$("." + currentSection + "Remember").click();

			//Copy first and last name accross to the other forms
			if(currentSection == "contact_data_form") {
				$scope.transferFirstAndLastName();

				//Check the users email
				//This is not currently working in Moltin!
				/*if(!$scope.userEmailChecked) {

					$scope.userEmailCheckInProgress = true;

					function userEmailCheckCallback(data) {
						console.log("Time to check the email");
						$scope.userEmailCheckInProgress = false;
						$scope.userEmailChecked = true;
					}

					function userEmailCheckFailure() {
						console.log("Failed to check email");
						$scope.userEmailCheckInProgress = false;
						$scope.userEmailChecked = true;
					}

					remoteRequestService.findCustomerByEmail($scope.order.email, userEmailCheckCallback, userEmailCheckFailure);

					return;

				}*/
			}

			$scope.sectionHeights[currentSection] = "0px";

			//$scope.currentSectionNumber++;

			$scope.completedSections.push(currentSection);

			var sectionIndex;

			if(!$scope.billingSameAsShipping) {
				sectionIndex = $scope.sectionOrder.indexOf(currentSection);
				currentSection = $scope.sectionOrder[sectionIndex+1];
			} else {
				sectionIndex = $scope.sectionOrderWithNoBilling.indexOf(currentSection);
				currentSection = $scope.sectionOrderWithNoBilling[sectionIndex+1];
			}


			//Track travel into section
			if($scope.trackedSections.indexOf(currentSection) == -1) {
				$scope.trackedSections.push(currentSection);
				eventTracking.navigateCheckout(currentSection);
			}


			$scope.sectionHeights[currentSection] = "1500px";

			if(currentSection == "delivery_options") {
				$scope.calculateDeliveryOptions();
			} else if(currentSection == "payment") {
				calculateAndRunPaymentOptions();
			}


		} else {

			if(currentSection == "shipping_address_form") {
				$scope.shippingShowAddressFields = true;
			} else if(currentSection == "billing_address_form") {
				$scope.billingShowAddressFields = true;
			}

		}

		$scope.scrollToSection(currentSection);
	}

	$scope.goBackSection = function() {
		$scope.sectionHeights[currentSection] = "0px";

		if(!$scope.billingSameAsShipping) {
			sectionIndex = $scope.sectionOrder.indexOf(currentSection);
			currentSection = $scope.sectionOrder[sectionIndex-1];
		} else {
			sectionIndex = $scope.sectionOrderWithNoBilling.indexOf(currentSection);
			currentSection = $scope.sectionOrderWithNoBilling[sectionIndex-1];
		}


		if(currentSection == "delivery_options") {
			$scope.calculateDeliveryOptions();
		} else if(currentSection == "payment") {
			calculateAndRunPaymentOptions();
		}

		$scope.completedSections.pop();

		$.each($scope.sectionHeights, function(key, value) {
			if(key == currentSection) {
				$scope.sectionHeights[key] = "1500px";
			}
		});

		$scope.scrollToSection(currentSection);
		//$scope.currentSectionNumber--;
	}

	$scope.scrollToSection = function(sectionId) {

		$timeout(function() {

			var offset = $("#" + sectionId).offset().top;

			if($(".mobile_menu_button").css("display") == "block") {
				offset -= $(".mobile_menu_button").height();
			}

			$("body").animate({scrollTop: offset});
		}, 750);

	}

	$scope.transferFirstAndLastName = function() {

		setAngularInput("#shipping_first_name", this.order.first_name);
		setAngularInput("#shipping_last_name", this.order.last_name);
		setAngularInput("#billing_first_name", this.order.first_name);
		setAngularInput("#billing_last_name", this.order.last_name);

	}

	function updateSectionDisplay() {

	}

	//Format a given address
	$scope.formatAddressForGrid = function(address) {

		var addressString = address.address_1 + ",  ";
		if(address.address_2 != "") addressString += address.address_2 + ",  ";
		addressString += address.city + ",  " + address.county + ",  " + address.postcode + ",  " + address.country.value;

		return addressString;
	}

	$scope.selectSavedAddress = function(shippingOrBilling, address) {

		eventTracking.selectSavedAddress();

		if(shippingOrBilling == "shipping") {


			setAngularInput("#shipping_address", address.address_1);
			setAngularInput("#shipping_address_second", address.address_2);
			setAngularInput("#shipping_city", address.city);
			setAngularInput("#shipping_postcode", address.postcode);
			setAngularInput("#shipping_state", address.county);
			setAngularInput("#shipping_phone", address.phone);

			//Hack to fix digest in progress bug
			$timeout(function() {
				$('#shipping_country option[value="' + address.country.data.code + '"]').prop('selected', true).trigger("change");
				//$('#shipping_state option[value="' + address.county + '"]').prop('selected', true).trigger("change");
			}, 100);

			$scope.shippingShowAddressFields = true;

		} else {

			setAngularInput("#billing_address", address.address_1);
			setAngularInput("#billing_address_second", address.address_2);
			setAngularInput("#billing_city", address.city);
			setAngularInput("#billing_postcode", address.postcode);
			setAngularInput("#billing_state", address.county);
			setAngularInput("#billing_phone", address.phone);

			//Hack to fix digest in progress bug
			$timeout(function() {
				$('#billing_country option[value="' + address.country.data.code + '"]').prop('selected', true).trigger("change");
				//$('#billing_state option[value="' + address.county + '"]').prop('selected', true).trigger("change");
			}, 100);

			$scope.billingShowAddressFields = true;

		}
	}

	//Filter counties for county dropdown based on country selection
	$scope.filterStates = function(shippingOrBilling) {

		if(shippingOrBilling == "shipping") {

			switch($scope.order.shipping_country) {
				case "GB":
					$scope.shippingAvailableCounties = $scope.ukCounties;
					break;
				default:
					$scope.shippingAvailableCounties = [];
					break;
			}

		} else {

			switch($scope.order.billing_country) {
				case "GB":
					$scope.billingAvailableCounties = $scope.ukCounties;
					break;
				default:
					$scope.billingAvailableCounties = [];
					break;
			}			

		}
		
	}

	$scope.filterBillingStates = function() {

	}

	$scope.calculateDeliveryOptions = function() {

		//var oldDeliveryArray = $scope.availableDeliveries;

		//if($scope.availableDeliveries == undefined) oldDeliveryArray = [];

		$scope.availableDeliveries = [];

		if($scope.order.shipping_country !== "GB") {
			$scope.availableDeliveries.push("shipping");

			//Get international shipping cost
			for(var i = 0; i < $scope.countryList.length; i ++) {
				if($scope.countryList[i].code == $scope.order.shipping_country) {
					$scope.internationalShippingCost = $scope.countryList[i].cost;
					break;
				}
			}

			$scope.availableDeliveries.push("collect");
		} else {
			if($scope.app_qualifiedForFreeDelivery) $scope.availableDeliveries.push("free");
			else $scope.availableDeliveries.push("next-day");
			$scope.availableDeliveries.push("collect");
		}


		switch($scope.availableDeliveries.length) {
			case 1:
				$scope.deliverySpanSize = "span12";
			break;
			case 2:
				$scope.deliverySpanSize = "span6";
			break;
			case 3:
				$scope.deliverySpanSize = "span4";
			break;
			case 4:
				$scope.deliverySpanSize = "span3";
			break;
		}

		/*var deliveryNoLongerAvailable = false;

		if(oldDeliveryArray.length !== $scope.availableDeliveries) deliveriesHaveChanged = true;
		else {
			for(var i = 0; i < oldDeliveryArray.length; i++) {
				if(oldDeliveryArray[i] !== $scope.availableDeliveries[i]) {
					deliveriesHaveChanged = true;
					break;
				}
			}
		}*/

		if($scope.availableDeliveries.indexOf($scope.chosenDelivery) == -1 || $scope.chosenDelivery == null) $scope.chosenDelivery = $scope.availableDeliveries[0];

		if($scope.chosenDelivery == "shipping") {
			$scope.deliveryPrice = $scope.internationalShippingCost;
		} else {
			$scope.deliveryPrice = $scope.deliveryCosts[$scope.chosenDelivery];
		}
		
	}

	$scope.chooseDelivery = function(deliveryType) {

		$scope.chosenDelivery = deliveryType;

		if(deliveryType == "shipping") {
			$scope.deliveryPrice = $scope.internationalShippingCost;
		} else {
			$scope.deliveryPrice = $scope.deliveryCosts[deliveryType];
		}

		//$scope.$apply();
	}

	$scope.getFinalTotalWithDelivery = function() {
		return $scope.prettyFormatPrice($scope.userCart.totals.post_discount.raw.with_tax + $scope.deliveryPrice);
		
	}

	$scope.getFinalTotalWithDeliveryNoPretty = function() {
		return $scope.userCart.totals.post_discount.raw.with_tax + $scope.deliveryPrice;
	}


	function calculateAndRunPaymentOptions() {

		$scope.paymentSpanSize = "span6";
		$scope.availablePayments = ["stripe", "paypal"];

		//Instantiate skeuocard
		bindCard();
		$scope.setupPaypalButton();

	}

	$scope.choosePaymentMethod = function(method) {
		$scope.paymentChosen = true;
		$scope.chosenPayment = method;

		eventTracking.choosePaymentMethod(method);

		bindCard();
	}

	$scope.changePaymentMethod = function() {
		$scope.chosenPayment = null; 
		$scope.paymentChosen = false; 
		$scope.applyAngularScope();
	}

	function bindCard() {

		function checkValidity() {
			/*console.log("checking");
			$scope.cardValid = $scope.card.isValid();*/
		}

		if(!$scope.skeuocardInitialised) {
			$scope.card = new Skeuocard($("#skeuocard"), {
				debug: true,
				dontFocus: true
			});

			$scope.skeuocardInitialised = true;

			/*window.setTimeout(function() {
				$(".group4").bind("change paste keyup", checkValidity);
			}, 100);

			$("#cc_exp_month").change(checkValidity);
			$("#cc_exp_year").change(checkValidity);
			$("#cc_name").change(checkValidity);
			$("#cc_cvc").change(checkValidity);*/

		}

	}

	$scope.getOrderJson = function() {
		var order = $scope.order;

		var shipping_address_second = "";
		var billing_address_second = "";

		if(order.shipping_address_second !== undefined) shipping_address_second = order.shipping_address_second;
		if(order.billing_address_second !== undefined) billing_address_second = order.billing_address_second;

		var shippingAddress = {
			first_name: order.first_name,
			last_name:  order.last_name,
			address_1:  order.shipping_address,
			address_2:  shipping_address_second,
			city:       order.shipping_city,
			county:     order.shipping_state,
			country:    order.shipping_country,
			postcode:   order.shipping_postcode,
			phone:      $("#shipping_phone").val()
		};

		if($scope.billingSameAsShipping) {
			billingAddress = shippingAddress;
		} else {
			billingAddress = {
				first_name: order.first_name,
				last_name:  order.last_name,
				address_1:  order.billing_address,
				address_2:  billing_address_second,
				city:       order.billing_city,
				county:     order.billing_state,
				country:    order.billing_country,
				postcode:   order.billing_postcode,
				phone:      $("#billing_phone").val()
			}
		}

		var shippingId = $.grep($scope.allShippingMethods, function(e){ return e.slug == $scope.chosenDelivery; })[0].id;

		//var paymentSlug = $scope.chosenPayment;
		//DEBUG ALWAYS DUMMY
		//var paymentSlug = 'dummy';

		//if($scope.chosenPayment == "stripe") paymentSlug = "dummy";
		if($scope.chosenPayment == "stripe") paymentSlug = "stripe";
		else if($scope.chosenPayment == "paypal") paymentSlug = "paypal-express";

		//Customer id should be used here if one provided
		var orderJson = {
			customer: {
				first_name: order.first_name,
				last_name: order.last_name,
				email: order.email
			},
			shipping: shippingId,
			gateway: paymentSlug,
			bill_to: billingAddress,
			ship_to: shippingAddress
		}

		return orderJson;
	}

	$scope.convertCartToOrder = function(orderJson, callback) {
		moltinApiService.createOrderFromCart(orderJson, callback, $scope.paymentErrorCallback);
	}

	//Check the stripe card is okay and continue
	$scope.checkStripeCardAndDisplayConfirmation = function() {

		//stripeAuthError
		//stripeErrorInfo

		$scope.stripeAuthError = false;

		//$scope.stripeGotoConfirmation();

		
		//DEBUG RECOMMENT THIS
		if(this.card.isValid()) {
			$scope.stripeErrorInfo = "";
			$scope.stripeGotoConfirmation();
		} else {
			$scope.stripeAuthError = true;
			$scope.stripeErrorInfo = "Please ensure all fields on the card are filled out correctly before continuing";
		}

	}

	$scope.stripeGotoConfirmation = function() {

		$scope.preparingOrder = true;

		function orderCreatedCallback(order) {
			$scope.storedOrderId = order.id;


			$timeout(function() {
				remoteRequestService.getOrderSummary($scope.storedOrderId, orderRetrievedCallback, $scope.paymentErrorCallback);
			}, $scope.longWaitOrderTimeout);
		}

		function orderRetrievedCallback(order) {
			$scope.moltinOrder = order.data.result;
			$scope.displayOrderConfirmation();
			$scope.applyAngularScope();
		}

		var orderJson = $scope.getOrderJson();

		moltinApiService.createOrderFromCart(orderJson, orderCreatedCallback, $scope.paymentErrorCallback);
	}

	//Load back in paypal order
	$scope.paypalLoadOrder = function(callback) {

		function orderCreatedCallback(order) {
			$scope.moltinOrder = order.data.result;
			$scope.paypalReplaceOrderDetails(callback);
			//callback();
			$scope.applyAngularScope();

			//Scroll page into view after redirect
			var offset = $(".confirmAccordionWrapper").offset();
			offset.left -= 20;
			offset.top -= 20;

			$('html, body').animate({
			    scrollTop: offset.top,
			    scrollLeft: offset.left
			});

		}

		remoteRequestService.getOrderSummary($scope.storedOrderId, orderCreatedCallback, $scope.paymentErrorCallback);

	}

	$scope.paypalReplaceOrderDetails = function(callback) {

		//Reload all the moltin order details back into the form thanks to paypals lovely redirect system

		var mol = this.moltinOrder;

		this.order.first_name = mol.customer.data.first_name;
		this.order.last_name = mol.customer.data.last_name;	
		this.order.email = mol.customer.data.email;
		this.order.email_repeat = mol.customer.data.email;

		this.order.shipping_first_name = mol.ship_to.data.first_name;
		this.order.shipping_last_name = mol.ship_to.data.last_name;
		this.order.shipping_address = mol.ship_to.data.address_1;
		this.order.shipping_address_second = mol.ship_to.data.address_2;
		this.order.shipping_city = mol.ship_to.data.city;
		this.order.shipping_state = mol.ship_to.data.county;
		this.order.shipping_country = mol.ship_to.data.country.data.code;
		this.order.shipping_postcode = mol.ship_to.data.postcode;
		this.order.shipping_phone = mol.ship_to.data.phone;

		this.shippingShowAddressFields = true;

		if(mol.bill_to.data.address_1 == mol.ship_to.data.address_1) this.billingSameAsShipping = true;
		else {
			this.billingSameAsShipping = false;
			this.billingShowAddressFields = false;
			this.order.billing_first_name = mol.bill_to.data.first_name;
			this.order.billing_last_name = mol.bill_to.data.last_name;
			this.order.billing_address = mol.bill_to.data.address_1;
			this.order.billing_address_second = mol.bill_to.data.address_2;
			this.order.billing_city = mol.bill_to.data.city;
			this.order.billing_state = mol.bill_to.data.county;
			this.order.billing_country = mol.bill_to.data.country.data.code;
			this.order.billing_postcode = mol.bill_to.data.postcode;
			this.order.billing_phone = mol.bill_to.data.phone;
		}

		if(mol.gateway.data.slug == "paypal-express") this.chosenPayment = "paypal"
		else if(mol.gateway.data.slug == "stripe" || mol.gateway.data.slug == "dummy") this.chosenPayment = "stripe";

		$scope.chosenDelivery = mol.shipping.data.slug;

		if($scope.chosenDelivery == "shipping") {
			$scope.deliveryPrice = $scope.internationalShippingCost;
		} else {
			$scope.deliveryPrice = $scope.deliveryCosts[$scope.chosenDelivery];
		}

		$scope.applyAngularScope();

		callback();
	}

	//When cancelling at paypal level, go back
	$scope.displayPaymentMethod = function(callback) {
		currentSection = "payment";

		$scope.setupPaypalButton();

		if($scope.billingSameAsShipping) {
			$scope.completedSections = $scope.sectionOrderWithNoBilling.slice(0, $scope.sectionOrderWithNoBilling.indexOf(currentSection));
		} else {
			$scope.completedSections = $scope.sectionOrder.slice(0, $scope.sectionOrder.indexOf(currentSection));
		}

		$.each($scope.sectionHeights, function(key, value) {
			if(key == currentSection) {
				$scope.sectionHeights[key] = "1500px";
			} else {
				$scope.sectionHeights[key] = "0px";
			}
		});
	}

	//Display the order confirmation box
	$scope.displayOrderConfirmation = function(callback) {

		$scope.preparingOrder = false;

		currentSection = "confirm";

		if($scope.billingSameAsShipping) {
			$scope.completedSections = $scope.sectionOrderWithNoBilling.slice(0, $scope.sectionOrderWithNoBilling.indexOf(currentSection));
		} else {
			$scope.completedSections = $scope.sectionOrder.slice(0, $scope.sectionOrder.indexOf(currentSection));
		}

		$.each($scope.sectionHeights, function(key, value) {
			if(key == currentSection) {
				$scope.sectionHeights[key] = "1500px";
			} else {
				$scope.sectionHeights[key] = "0px";
			}
		});

		$scope.finalPaymentVerfiedText = "";
		$scope.finalPaymentError = false;
		$scope.finalPaymentErrorText = "";

		if($scope.chosenPayment == "paypal") {
			$scope.finalPaymentVerfiedText = "Payment will be charged to your Paypal account";
		} else if($scope.chosenPayment == "stripe") {
			$scope.finalPaymentVerfiedText = "Payment will be charged to your card, ending in " + $("#cc_number").val().substr(14,15);
		}

	}

	$scope.loadMoltinOrder = function() {

	}

	//Click to complete the order when ready
	$scope.completeOrder = function() {
		//Direct to final purchase here

		eventTracking.completeOrder();

		$scope.showPaymentOverlay();

		if($scope.chosenPayment == "paypal") {
			$scope.paypalCompleteOrder();
		} else if($scope.chosenPayment == "stripe") {
			$scope.stripeCompleteOrder();
		}
	}

	//Complete order for stripe
	$scope.stripeCompleteOrder = function() {

		var orderId = $scope.storedOrderId;

		var firstName = $scope.order.first_name; 
		var lastName = $scope.order.last_name;

		var cardData = {
			data: {
				first_name: firstName,
				last_name: lastName,
				number: $("#cc_number").val(),
				expiry_month: $("#cc_exp_month").val(),
				expiry_year: $("#cc_exp_year").val(),
				cvv: $("#cc_cvc").val()
			}
		};

		/*var cardData = {
		    "data": {
		        "first_name": "Dave",
		        "last_name": "Daveson",
		        "number": "4444444444444444",
		        "expiry_month": "12",
		        "expiry_year": "2012",
		        "cvv": "088"
		    }
		};*/

		/*var cardData = {
	        "data": {
	            "first_name": "John",
	            "last_name": "Doe",
	            "number": "4242424242424242",
	            "expiry_month": "08",
	            "expiry_year": "2020",
	            "cvv": "123"
	        }
	    };*/

	    var accountCreationData = {
	    	createAccount: $scope.createAccount,
	    	useCustomPassword: $scope.createAccountCustomPassword,
	    	customPassword: $scope.customPassword,
	    	signupNewsletter: $scope.signupNewsletter
	    };

	    if($scope.loggedIn) accountCreationData.createAccount = false;


	    function delayedPayment() {
			remoteRequestService.payForOrderOnsite(orderId, cardData, accountCreationData, $scope.paymentCompleteCallback, $scope.finalPaymentErrorCallback);
	    }

	    $timeout(delayedPayment, $scope.waitOrderTimeout);

	}

	$scope.paypalCompleteOrder = function() {

		function paypalCompleteCallback(response) {
			orderId = response.data.result.id;
			moltinApiService.clearCartAndGotoReceipt(orderId, $scope.redirectToReceiptCallback, $scope.redirectToReceiptCallback);
		}

		function paypalCompleteFailureCallback() {
			console.log("failed");
		}

		var orderId = $scope.storedOrderId;
		var accountCreationData = {
	    	createAccount: $scope.createAccount,
	    	useCustomPassword: $scope.createAccountCustomPassword,
	    	customPassword: $scope.customPassword,
	    	signupNewsletter: $scope.signupNewsletter
	    };

	    if($scope.loggedIn) accountCreationData.createAccount = false;

		//remoteRequestService.paypalCompleteOrder(orderId, accountCreationData, paypalCompleteCallback, paypalCompleteFailureCallback);

	    var total = $scope.moltinOrder.total;

	    function delayedPayment() {
			remoteRequestService.paypalPayForOrder(total, $scope.paypalToken, $scope.paypalPayerId, orderId, accountCreationData, $scope.paymentCompleteCallback, $scope.finalPaymentErrorCallback);
	    }

	    $timeout(delayedPayment, $scope.waitOrderTimeout);

	}


	$scope.paymentCompleteCallback = function(response) {

		if(!response.data.status) {
			//Extra error capture (not a major error but back from moltin)
			$scope.finalPaymentErrorCallback(response.data.error);
		} else {

			//Navigate to the receipt page to display order results
			var orderId; 

			if($scope.chosenPayment == "stripe") {
				orderId = response.data.result.order.id;
			} else if($scope.chosenPayment == "paypal") {
				orderId = response.data.result.id;
			}

			moltinApiService.clearCartAndGotoReceipt(orderId, $scope.redirectToReceiptCallback, $scope.redirectToReceiptCallback);
			
		}
	}

	$scope.finalPaymentErrorCallback = function(response) {
		$scope.finalPaymentError = true;

		if(response != undefined) $scope.finalPaymentErrorText = response + " Please try a different card or method, or check your customer & billing details";
		else {
			$scope.finalPaymentErrorText = "Could not complete purchase, please ensure you have sufficient funds in your account to make the payment and try again";
		}
		
		//$scope.$apply();

		$scope.applyAngularScope();

		vex.close();
	}

	$scope.redirectToReceiptCallback = function(orderId) {

		if(!$scope.redirectedToReceipt) {

			$scope.redirectedToReceipt = true;

			vex.close();
			$scope.showPaymentCompleteOverlay();

			$scope.trackConversion(orderId);
			
		}

	}

	$scope.trackConversion = function(orderId) {

		if(!$scope.conversionTracked) {

			var cartAmount = "0";

			try {
				cartAmount = $scope.moltinOrder.total.toString();
			} catch(e) {}

			//Fire all tracking events
			//eventTracking.successfulPayment(cartAmount);

			//Track conversion with FB
	        try {
	            fbq('track', 'Purchase', {
	                value: cartAmount,
	                currency: 'GBP'
	            });
	        } catch(e) {}

	        //Track conversion with google
	        try {
	            ga('send', {
	                hitType: 'event',
	                eventCategory: 'checkout_process',
	                eventAction: 'conversion',
	                eventValue: cartAmount
	            });
	        } catch(e) {}

	        try {
	        	ga('ecommerce:addTransaction', {
				  'id': orderId,
				  'affiliation': 'Edited Fresh Webstore',
				  'revenue': cartAmount
				});

				var cart = $scope.userCart.contents;

				for(var key in cart) {

					try {
						
						var item = cart[key];

						ga('ecommerce:addItem', {
							'id': orderId,                     // Transaction ID. Required.
							'name': item.name,    // Product name. Required.
							'sku': item.sku,                 // SKU/code.
							'category': item.category.value,         // Category or variation.
							'price': item.price,
							'quantity': item.quantity
						});

					} catch(e) {}


				}

				ga('ecommerce:send');
	        } catch(e) {}

	        //Track conversion with Bing
	        try {
	            window.uetq = window.uetq || [];
	            window.uetq.push({ 'ec':'checkout_process', 'ea':'conversion', 'el':'', 'ev': cartAmount });
	        } catch(e) {}
					

			//Track conversion with perfect audience
			window._pq = window._pq || [];
			_pq.push(['track', 'conversion', {orderId: orderId, revenue: cartAmount}]);

			$scope.conversionTracked = true;

			$scope.goToReceipt(orderId);

		} else {
			$scope.goToReceipt(orderId);
		}


	}

	$scope.goToReceipt = function(orderId) {

		/*$scope.rawCartAmount = "£0.00";
		$scope.userCart = {
			total_items: 0
		};*/

		$scope.makeUserRequest({op: "clearCart"});

		$scope.applyAngularScope();
		//Wait before redirecting
		$timeout(function() {
			vex.close();

			/*$scope.rawCartAmount = "£0.00";
			$scope.userCart = {
				total_items: 0
			};*/
			//eventTracking.directToReceipt();
			
			$location.path('/receipt' + orderId);
		}, 4000);

	}

	$scope.paymentErrorCallback = function(error, response) {
		console.log("error");

		//$scope.paymentInProgress = false;

		$scope.paymentError = true;

		if(response != undefined) $scope.paymentErrorInfo = response + ", please try a different card or method, or check your customer & billing details";
		else {
			$scope.paymentErrorInfo = "Could not complete purchase, potential error contacting server, please try again";
		}
		
		//$scope.$apply();

		$scope.applyAngularScope();

		vex.close();

	}

	$scope.showPaymentOverlay = function() {
		console.log("showing payment overlay");

		vex.open({
			content: '<i class="fa fa-spinner fa-spin fa-4x paymentUpdateSpinner"></i><br><h4>Processing your order, just a moment...</h4><span>Please don\'t navigate away from the page</span>',
			showCloseButton: false,
			escapeButtonCloses: false,
			overlayClosesOnClick: false,
			appendLocation: 'body',
			contentClassName: 'vex-payment-content vex-content'
		})
	}

	$scope.showPaymentCompleteOverlay = function() {
		console.log("showing completed payment overlay");

		vex.open({
			content: '<i class="fa fa-check fa-4x vex-tick"></i><br><h4>Payment complete!</h4><span>Redirecting you to your receipt in 5 seconds</span>',
			showCloseButton: false,
			escapeButtonCloses: false,
			overlayClosesOnClick: false,
			appendLocation: 'body',
			contentClassName: 'vex-payment-content vex-content'
		})

	}

	//Go back section after you are at confirmation stage (reload form data if dealing with paypal redirect transaction)
	$scope.goBackFromConfirmation = function() {
		vex.dialog.confirm({
			message: 'You may need to re-enter or re-verify any payment information if you go back.. <b>continue?</b>',
			callback: function(value) {
				if(value) {
					$scope.goBackSection();
					$scope.applyAngularScope();

					eventTracking.goBackFromConfirm();
				}
			}
		});
	}

	//PAYPAL POST LOAD CHECKING
	//Check if paypal has just sent you back from here
	if($routeParams.paypal) {

	    $scope.storedOrderId = $routeParams.orderId;

	    $scope.createAccount = ($routeParams.createAccount == "true");
	    $scope.createAccountCustomPassword = ($routeParams.useCustomPassword == "true");
	    if($routeParams.customPassword != "undefined") {
		    $scope.customPassword = $routeParams.customPassword;
		    $scope.customPasswordRepeat = $routeParams.customPassword;
	    }
	    $scope.signupNewsletter = ($routeParams.signupNewsletter == "true");
	    $scope.paypalToken = $routeParams.token; 
	    $scope.paypalPayerId = $routeParams.PayerID;

	    $scope.chosenPayment = "paypal";

		if($routeParams.paypal == "success") {

			$scope.paypalLoadOrder($scope.displayOrderConfirmation);
		} else if($routeParams.paypal == "cancel") {
			$scope.paypalLoadOrder($scope.displayPaymentMethod);
		}

	} else {
		if($scope.userCart.total_items == 0) $location.path("/cart");
	}

	$scope.checkoutCartMode = true;


}]);
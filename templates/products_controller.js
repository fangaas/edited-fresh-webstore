app.controller('products_controller', ['$scope', '$routeParams', 'moltinApiService', '$location', '$timeout', 'eventTracking', 'editedDataStore', 'editedFurnitureModifierStore', function($scope, $routeParams, moltinApiService, $location, $timeout, eventTracking, editedDataStore, editedFurnitureModifierStore) {
	
    //Check if display category or product
    var productSlug = $routeParams.productSlug;
    var routeParams = $routeParams;
	$scope.displayProductsGrid = false;
	$scope.productGridLoaded = false;
	$scope.displayProductDetails = false;
	//Current category that we are viewing
	$scope.viewCategory;
	$scope.product = null;

	$scope.gridTitle = "";
	$scope.gridTitleBackgroundClass = "prodGrid-headerBlue";

	$scope.parentProduct;
	$scope.variants = [];
	$scope.variantArray = [];
	$scope.selectedVariant1;
	$scope.selectedVariant2;
	$scope.selectedVariant3;

	/*Variables for furniture modifiers*/
	$scope.furni_configurable = false;
	$scope.furni_config;
	$scope.furni_colourGroups = editedFurnitureModifierStore.colourGroups;
	$scope.furni_selections = [];
	$scope.furni_operations = {};
	$scope.furni_selectionMade = false;
	$scope.furni_selectionsFinished = false;

	$scope.furni_modifiers = {};

	$scope.filterType = "";

	$scope.loadingProductVariant = false;

	$scope.subVariants1;
	$scope.subVariants2;

	$scope.addProductQuantity = 1;

	//Controller for detail accordion heights
	$scope.accordion = {
		"description": true,
		"details": false,
		"delivery": false,
		"whyus": false
	};

	$scope.productHasVariants = false;

	//Only products with all selected variants can be put in cart
	$scope.productReadyForCart = false;

	//The current category object which holds paging details etc.
	$scope.currentCategoryObject = null;

	$scope.loadNextSetInProgress = false;

	$scope.loadMoreClicked = false;

	$scope.itemJustAdded = false;

	//Product out of stock reminder information
	$scope.reminder_signupNewsLetter = true;
	$scope.reminder_email = "";
	$scope.reminder_submitted = false;
	$scope.reminder_error = false;

	$scope.displayProductCategoryLink = true;

	$scope.structuredProductJson = {};

	$scope.relatedProductsLoading = false;
	$scope.relatedProductsDisplay = false;
	$scope.relatedProductsArray = [];
	$scope.relatedProductsDisplayAmount = 4;

	$scope.tooMuchQuantity = false;

	$scope.setMetaTags({
		title: "Loading... | Edited",
		description: "Offering a curated collection of home furnishings, lighting, artwork, cookware and soft furnishings specialising in design-led interiors."
	});

	$scope.addItemCallback = function() {

		$scope.itemJustAdded = true;
		$scope.applyAngularScope();

	}

	$scope.addItemFurnitureCallback = function() {

		$scope.itemJustAdded = true;
		$scope.applyAngularScope();

	}

	$scope.addItemToCartProduct = function(productId, productName, quantity, callback) {

		/*if($scope.furni_configurable) {

			if($scope.furni_selectionsFinished) {

				//Register furni selections here and save to local storage for display
				$scope.addItemToCart(productId, productName, quantity, $scope.addItemFurnitureCallback);

			}

		} else {
			$scope.addItemToCart(productId, productName, quantity, $scope.addItemCallback);
		}*/

		var existingCartQuantity = 0;

		for(var key in $scope.userCart.contents) {
			if($scope.product.title == $scope.userCart.contents[key].title) existingCartQuantity = $scope.userCart.contents[key].quantity;
		}

		if((quantity + existingCartQuantity) <= $scope.product.stock_level) {
			$scope.tooMuchQuantity = false;
			$scope.addItemToCart(productId, productName, quantity, $scope.addItemCallback);
			$scope.applyAngularScope();
		} else {
			$scope.tooMuchQuantity = true;
		}



	}




	$scope.redirectInErrorToHome = function() {

		$scope.$apply(function() {
			$location.path('');
		});

	}

	$scope.goStraightToCart = function() {

		eventTracking.goStraightToCart();

		$location.path('cart');

	}

	$scope.showProductGridHeader = function() {

		$timeout(function() {
			$scope.displayProductsGrid = true;
		}, 0);

	}


	//-----Load the related products to the current product----//
	$scope.loadRelatedProducts = function() {

		if(!this.product.isGroupedProduct !== undefined && !this.product.isGroupedProduct) {

			$scope.relatedProductsLoading = true;
			$scope.relatedProductsDisplay = true;

			var existingLoadedItems = this.loadedProductsObject[this.product.category_link];

			if(existingLoadedItems != undefined && existingLoadedItems.productsArray.length >= 4) {
				finshLoadingRelatedProducts(existingLoadedItems.productsArray)
			} else {

				var categoryId;

				for(var i = 0; i < $scope.allCategories.length; i ++) {

					if($scope.allCategories[i].title == $scope.product.category) {
						categoryId = $scope.allCategories[i].id;
						break;
					}

				}

				var filterObj = {
					category: categoryId,
					stock_status: 1,
					limit: 10
				};

				moltinApiService.getFilteredProducts(filterObj, finshLoadingRelatedProducts);
			}

		}

		function selectAndTranslateRelatedProducts(products) {

			var usedIndexes = [],
			finished = false,
			selectedProducts = [];

			while(!finished) {

				var index = Math.floor(Math.random()*products.length);

				if(usedIndexes.indexOf(index) == -1) {
					if($scope.product.title == products[index].title) usedIndexes.push(index); 
					else {
						selectedProducts.push(translateProductToReadableJson(products[index]));
						usedIndexes.push(index); 
					}
				}

				if(usedIndexes.length == products.length || selectedProducts.length == $scope.relatedProductsDisplayAmount) finished = true;

			}

			return selectedProducts;

		}

		function finshLoadingRelatedProducts(products) {

			$scope.relatedProductsArray = selectAndTranslateRelatedProducts(products);
			$scope.applyAngularScope();

			$timeout(function() {
				$scope.relatedProductsLoading = false;
				$scope.runImageLoader();
			}, 2000);

		}

	}



	$scope.selectVariant = function(variantCounter, selection) {
		
		if(variantCounter == 1) {
			$scope.selectedVariant1 = selection;	
			$scope.subVariants1 = $scope.selectedVariant1.subVariants;
			$scope.selectedVariant2 = null;
		} else if(variantCounter == 2) {
			$scope.selectedVariant2 = selection;
		}

		if($scope.subVariants1 == undefined && $scope.selectedVariant1 !== null) {
			var variant1slug = $scope.selectedVariant1.slug

			if(variant1slug == undefined) variant1slug = $scope.selectedVariant1.comboName.toLowerCase();

			var newSlug = $scope.parentProduct.slug + "-" + variant1slug;

			var variantCallback = function() {
				$location.path('/' + newSlug);	

				$("#variantSelect1").blur();

				$scope.loadingProductVariant = false;
			}

			$scope.loadingProductVariant = true;

			loadProductBackground(newSlug, variantCallback);

		} else if($scope.selectedVariant1 !== null && $scope.selectedVariant2 !== null) {
			//Proceed to next page
			var variant1slug = $scope.selectedVariant1.slug
			var variant2slug = $scope.selectedVariant2.slug

			if(variant1slug == undefined) variant1slug = $scope.selectedVariant1.comboName.toLowerCase();

			if(variant2slug == undefined) variant2slug = $scope.selectedVariant2.comboName.toLowerCase();

			var newSlug = $scope.parentProduct.slug + "-" + variant1slug + "-" + variant2slug;

			var variantCallback = function() {

				$timeout(function() {
					$location.path('/' + newSlug);
				}, 10);


				$("#variantSelect1").blur();
				$("#variantSelect2").blur();

				$scope.loadingProductVariant = false;
			}

			$scope.loadingProductVariant = true;

			loadProductBackground(newSlug, variantCallback);
			//

		}

		//if(variantCounter == 2) $scope.subVariants2 = $scope.selectedVariant2.subVariants;

	}

	$scope.selectFurnitureConfiguration = function(modifier, mode, id, group) {

		$scope.furni_selectionMade = true;

		if(mode == "chosenColour") {
			$scope.furni_operations[modifier].selectedColour = id;
			$scope.furni_operations[modifier].selectedColourGroup = group;
			$scope.furni_operations[modifier].mode = mode;
			$scope.furni_operations[modifier].selectionComplete = true;

			//$scope.product.

			var priceIncrease = $scope.furni_config.filter(function(el) {
				return el.modifier == modifier;
			})[0].colourGroups.filter(function(el) {
				return el.id == group;
			})[0].priceIncrease;

			$scope.furni_operations[modifier].priceIncrease = priceIncrease;

			$scope.product.rawPrice = $scope.product.originalPrice;

			for(var key in $scope.furni_operations) {
				$scope.product.rawPrice = $scope.product.rawPrice + $scope.furni_operations[key].priceIncrease;
				$scope.product.price = "£" + ($scope.product.rawPrice).toFixed(2);
			}

			if(localStorage["furniture_config"] == undefined) localStorage["furniture_config"] = JSON.stringify({});

			var localFurniConfig = JSON.parse(localStorage["furniture_config"]);

			localFurniConfig[$scope.product.slug] = $scope.furni_operations;

			localStorage["furniture_config"] = JSON.stringify(localFurniConfig);


		}

		var complete = true;
		for(var key in $scope.furni_operations) {
			if($scope.furni_operations[key] == false) complete = false;
		}

		$scope.furni_selectionsFinished = complete;

	}

	$scope.changeFurnitureConfigMode = function(modifier, mode, colourGroup) {
		
		if(mode == "selectColour") {
			$scope.furni_operations[modifier].mode = mode;
			$scope.furni_operations[modifier].colourGroup = colourGroup;
		} else if(mode == "selectSet") {
			$scope.furni_operations[modifier].mode = mode;
		} else if(mode == "chosenColour") {
			$scope.furni_operations[modifier].mode = mode;
		}

	}

	$scope.clickAccordion = function(which) {


		for(var key in $scope.accordion) {
			if(key == which) $scope.accordion[which] = !$scope.accordion[which];
			else $scope.accordion[key] = false;
		}

	}

	$scope.clickQuantity = function(val) {
		if(val == undefined || val < 1 || val > $scope.maxAmountThatCanBeAddedToCart) {
			$(".product-quantity").val(1);
			$scope.addProductQuantity = 1;
		}
	}

	function finishLoading() {

		$scope.runImageLoader();

	}

  	function determineWhichPageMode() {

  		$("body").animate({scrollTop:0});

  		var displayingProductGrid = false;
	  	for(var i = 0; i < $scope.allCategories.length; i ++) {

	  		var cur = $scope.allCategories[i];

	  		if(cur.slug == productSlug) {
	  			displayingProductGrid  = true;
	  			$scope.viewCategory = $scope.allCategories[i];

	  			if($scope.viewCategory.parent != null) $scope.gridTitle = $scope.viewCategory.parent.data.title + " - " + $scope.viewCategory.title;
	  			else $scope.gridTitle = $scope.viewCategory.title;

	  			break;
	  		} 

	  		if(cur['children']) {

	  			for(var j = 0; j < cur.children.length; j++) {
	  				if(cur.children[j].slug == productSlug) {
	  					displayingProductGrid = true;
	  					$scope.viewCategory = cur.children[j];
	  					
	  					if($scope.viewCategory.parent != null) $scope.gridTitle = $scope.viewCategory.parent.data.title + " - " + $scope.viewCategory.title;
  						else $scope.gridTitle = $scope.viewCategory.title;

	  					break;
	  				}
	  			}
	  		}
	  	}


  		if(displayingProductGrid) {

	  		$scope.setCatForSidebar($scope.gridTitle);
  			loadProductsForGrid(productSlug);

  			if($scope.viewCategory != undefined) eventTracking.navigateToCategory($scope.viewCategory.title);
  			
  		} 
  		else loadProductForDisplay(productSlug);	  	

	  	/*function continueToProductRender() {
	  		if(displayingProductGrid) productsGridView();
	  		else productDetailsView();

	  		if(displayingProductGrid) loadProductsForGrid();
	  		else productDetailsView();
	  	}*/

	  	/*if($scope.allProducts == null) {
			$scope.$watch( "allProducts" , function(n,o){
				if(n == o) return;
				continueToProductRender();
			});  
		} else {
			continueToProductRender();
		}*/

  	}

  	var debug = false;

  	//Ensure categories have loaded before determining, unless using filtered display
  	if(routeParams.productSlug == "filter") {
  		filteredView(routeParams);
  	} else {
	  	if(!debug) {
		  	if($scope.allCategories == null) {
				$scope.$watch( "allCategories" , function(n,o){
					if(n == o) return;
					determineWhichPageMode();
				});  
			} else {
				determineWhichPageMode();
			}
	  	}
  	}

	function appendProductToLoadedArray(newProd) {
		var exists = false;
		for(var j = 0; j < $scope.allProductsArray.length; j ++) {

			if($scope.allProductsArray[j] !== undefined && newProd.title == $scope.allProductsArray[j].title) {
				exists = true;
				break;
			} 
		}
		if(!exists) $scope.allProductsArray.push(newProd);
	}

	/*function findAndDisplayCategoryCurrentlyViewingForProduct(slug) {
		for(var i = 0; i < $scope.allCategories.length; i ++) {
			if($scope.allCategories[i].slug == slug) {
				$scope.viewCategory = $scope.allCategories[i];
				break;
			}
		}	
	}*/

	function filteredView(routeParams) {

		$("body").animate({scrollTop:0});

		var filterType, filterVal;

		if(routeParams.type == "search") {
			filterType = "title";
			$scope.filterType = "search";
			$scope.gridTitle = "Search: " + routeParams.val
			filterVal = routeParams.val;
			runSearchRequest(filterType, filterVal);
		} else if(routeParams.type == "brand") {
			$scope.filterType = filterType = "brand";
			$scope.gridTitle = "Brand: " + routeParams.val;

			filterVal = findBrandId(routeParams.val);
			runFilterRequest(filterType, filterVal);

			/*if($scope.allBrands !== null) {
				filterVal = findBrandId(routeParams.val);
				runFilterRequest(filterType, filterVal);
			} else {
				moltinApiService.getBrands(function(brands) {
					$scope.allBrands = brands;
					filterVal = findBrandId(routeParams.val);
					runFilterRequest(filterType, filterVal);
				});
			}*/

		} else if(routeParams.type == "sale") {
			filterType = "collection";
			filterVal = "1273088872066056876";

			$scope.gridTitle = "Edited Sale";
			$scope.gridTitleBackgroundClass = "prodGrid-headerOrange";

			$scope.filterType = "sale";

			runFilterRequest(filterType, filterVal, false);
		}

		function findBrandId(brandSlug) {

			var brandId;

			/*for(var i = 0; i < $scope.allBrands.length; i++) {
				if($scope.allBrands[i].slug == brandSlug) {
					brandId = $scope.allBrands[i].id;
					break;
				}
			}*/

			for(var i = 0; i < editedDataStore.allBrands.length; i++) {
				if(editedDataStore.allBrands[i].slug == brandSlug) {
					brandId = editedDataStore.allBrands[i].id;
					break;
				}
			}

			return brandId;
		}

		function runFilterRequest(filterType, filterVal, doGroupings) {

			if($scope.loadedProductsObject[filterVal] !== undefined) {
				$scope.showProductGridHeader();
				productsGridView($scope.loadedProductsObject[filterVal]);
			} else {

				var filterObj = {
					"limit": 100
				};

				filterObj[filterType] = filterVal;

				var getFilteredProductsCallback = function(products) {

					var groupedProducts;

					if(doGroupings !== false) {
						groupedProducts = groupProducts(products, $scope);
					} else {
						groupedProducts = products;
					}

					for(var i = 0; i < groupedProducts.length; i++) {
						appendProductToLoadedArray(groupedProducts[i]);
					}

					productsGridView(groupedProducts);

					$scope.loadedProductsObject[filterVal] = groupedProducts;

				}

				$scope.showProductGridHeader();

				moltinApiService.getFilteredProducts(filterObj, getFilteredProductsCallback);
				
			}


		}

		function runSearchRequest(filterType, filterVal) {

			var filterObj = {};

			filterObj[filterType] = filterVal;

			var getFilteredProductsCallback = function(products) {
				var groupedProducts = groupProducts(products, $scope);

				for(var i = 0; i < groupedProducts.length; i++) {
					appendProductToLoadedArray(groupedProducts[i]);
				}

				productsGridView(groupedProducts);

			}

			$scope.showProductGridHeader();

			moltinApiService.getSearchProducts(filterObj, getFilteredProductsCallback);

		}


	}

	//Load up the products for the grid display
	function loadProductsForGrid(categorySlug) {
		console.log("loading for categoryslug: " + categorySlug);

		console.log($scope.viewCategory);

		var categoryProducts = null;

		$scope.loadMoreClicked = false;

		//Check if the products for this category have already been loaded
		if(categorySlug in $scope.loadedProductsObject) {
			$scope.currentCategoryObject = $scope.loadedProductsObject[categorySlug];
			categoryProducts = $scope.loadedProductsObject[categorySlug].productsArray;
			$scope.showProductGridHeader();
			
			productsGridView(categoryProducts, true);
			console.log("Products for this category already loaded");
		} else {

			var displayCategoryCallback = function(groupedCategoryProducts) {
				productsGridView(groupedCategoryProducts, false);
			}

			$scope.showProductGridHeader();

			loadCategoryProducts($scope.viewCategory.id, categorySlug, displayCategoryCallback);

		}
	}

	

	$scope.loadNextSetOfProducts = function(limit) {

		if($scope.loadNextSetInProgress) return;

		if(limit == undefined) $scope.loadMoreClicked = true;

		var displayCategoryCallback = function(groupedCategoryProducts) {
			productsGridView(groupedCategoryProducts, false, true);
		}

		$scope.loadNextSetInProgress = true;

		loadCategoryProducts($scope.viewCategory.id, $scope.viewCategory.slug, displayCategoryCallback, limit);

	}

	//Remove products that have the default product image
	function filterProducts(products) {

		for(var i = 0; i < products.length; i++) {

			var sampleProductJson = translateProductToReadableJson(products[i]);

			if(sampleProductJson.image.indexOf("no_product.png") !== -1 || sampleProductJson.image == "") {
				products.splice(i, 1);
				i = -1;
			}

		}

		return products;

	}


	function loadCategoryProducts(categoryId, categorySlug, extraCallback, limit) {
		
		var getGridProductsCallback = function(products, pagingDetails) {

			var groupedCategoryProducts;

			if($scope.loadedProductsObject[categorySlug] == undefined) {
				$scope.loadedProductsObject[categorySlug] = {
					productsArray: [],
					hasNext: false,
					totalShown: 0,
					totalNotHidden: 0
				}
			}

			$scope.loadedProductsObject[categorySlug].hasNext = (pagingDetails.links.next !== false);
			$scope.loadedProductsObject[categorySlug].totalShown = pagingDetails.to;

			$scope.currentCategoryObject = $scope.loadedProductsObject[categorySlug];

			var groupedCategoryProducts = groupProducts(products, $scope);

			//var groupedCategoryProducts = filterProducts(unfilteredGroupedCategoryProducts);

			$scope.loadedProductsObject[categorySlug].productsArray = $scope.loadedProductsObject[categorySlug].productsArray.concat(groupedCategoryProducts);

			for(var i = 0; i < groupedCategoryProducts.length; i++) {
				appendProductToLoadedArray(groupedCategoryProducts[i]);
			}

			//$scope.loadedProductsObject[categorySlug].totalNotHidden = $scope.loadedProductsObject[categorySlug].totalShown - $scope.loadedProductsObject[categorySlug].productsArray.length;

			//No longer any need to filter
			if($scope.loadedProductsObject[categorySlug].productsArray.length < 12 && $scope.currentCategoryObject.hasNext) {
				var difference = 12 - $scope.loadedProductsObject[categorySlug].productsArray.length;
				$scope.loadNextSetOfProducts(difference);
			} else if($scope.loadMoreClicked && $scope.currentCategoryObject.hasNext && window.innerWidth > 767) {
				$timeout($scope.loadNextSetOfProducts, 200);
				//$scope.loadNextSetOfProducts();
			}

			/*if($scope.loadMoreClicked && $scope.currentCategoryObject.hasNext) {
				$timeout($scope.loadNextSetOfProducts, 200);
			}*/

			//var allProductsForCategory = $scope.loadedProductsObject[categorySlug].productsArray;
			//You will need to take the grouped products for display otherwise no 
			
			if(extraCallback != null) extraCallback(groupedCategoryProducts);
		}

		var filterObj = {
			category: categoryId
		};

		if($scope.currentCategoryObject !== null && $scope.currentCategoryObject.hasNext) {

			filterObj["offset"] = $scope.currentCategoryObject.totalShown;

			if(limit !== undefined) filterObj["limit"] = limit;

		}

		moltinApiService.getFilteredProducts(filterObj, getGridProductsCallback);

	}

	function loadProductBackground(productSlug, callback) {
		var product, productAlreadyLoaded = false;

		//Check its not a grouped product, if so then load it up
		if($scope.groupedProducts[productSlug] !== undefined) {
			product = $scope.groupedProducts[productSlug];
			productAlreadyLoaded = true;
		} else {
			for(var i = 0; i < $scope.allProductsArray.length; i ++) {
				if(sanitiseSlug($scope.allProductsArray[i].slug) == productSlug) {
					product = $scope.allProductsArray[i];
					productAlreadyLoaded = true;
					break;
				}
			}
		}


		if(productAlreadyLoaded) {
			//productDetailsView(product, true);
			callback(product, true);
		} else {

			var desanSlug = deSanitiseSlug(productSlug);

			var filterObj = {
				slug: desanSlug
			};

			var getDisplayProductCallback = function(products) {
				var singleProduct = products[0];
				appendProductToLoadedArray(singleProduct);
				//productDetailsView(singleProduct, false);
				callback(singleProduct, false);

				//Product is loaded now need to go off and try to group them together, good fucking luck
				//No longer needed as doing manual variants list
				/*var cat = singleProduct.category.data,
				catObject = cat[Object.keys(cat)[0]],
				catId = catObject.id;
				//ID may not be right

				if($scope.loadedProductsObject[catObject.slug]) {
					groupProducts($scope.loadedProductsObject[catObject.slug], $scope) 
				} else {
					loadCategoryProducts(catId, catObject.slug, null);
				}*/

			}


			moltinApiService.getFilteredProducts(filterObj, getDisplayProductCallback);

		}
		
	}

	//Load up the specific product for display
	function loadProductForDisplay(productSlug) {
		
		loadProductBackground(productSlug, productDetailsView);

	}

	function compareSlugs(slug1, slug2) {
		var matchCount = 0,
		slugArray1 = slug1.split("-"),
		slugArray2 = slug2.split("-");

		for(var j = 0; j < slugArray1.length; j ++) {
			if(slugArray1[j] == slugArray2[j]) matchCount++;
			else break;
		}

		return matchCount;
	}

	function groupProducts(productsArray, $scope) {

		console.log("going to group ze products");


		var groupedKeys = [];

		for(var i = 0; i < productsArray.length; i++) {

			var curProd = productsArray[i];

			var curSlug = curProd.slug;

			if(!curProd.isGroupedProduct) {
				for(var key in $scope.groupedProducts) {

					var curGroupProd = $scope.groupedProducts[key];

					if(compareSlugs(sanitiseSlug(curProd.slug), key) >= curGroupProd.matchLength ) {
						//This product makes it in

						//Put parent product in group
						if(groupedKeys.indexOf(key) == -1) {
							productsArray.push(curGroupProd);
							groupedKeys.push(key);
							//pull through description and that
						}

						//Remove child product from group
						productsArray.splice(i, 1);

						i = -1;

					}

				}
			}


		}

		/*$scope.groupedProductsObject = {
			"product-slug": [{
				childObjects
			}]
		}*/

		//Take all products for this category, group them together 
		//You need to return the array with the grouped up products removed and only the parent products and lonely products left
		return productsArray;
	}

	function sanitiseSlug(slug) {

		//return slug.replaceAll("\\.", "").replaceAll("/-", "").replaceAll("/", "-").replaceAll("!", "").replaceAll("&", "and").replaceAll("'", "").replaceAll("(", "").replaceAll(")", "").replaceAll(",","");
		return slug;

	}

	function extraSanitise(slug) {



	}

	function deSanitiseSlug(slug) {

		//return slug.replaceAll("and", "&");
		return slug;

	}

	function translateProductToReadableJson(prod) {

		var newSlug = sanitiseSlug(prod.slug);

		var dataJson,
		description;

		try {
			var dataJson = JSON.parse(prod.description.split("%endjson%")[0]);
			var description = prod.description.split("%endjson%")[1];
		} catch(e) {
			dataJson = {
				mainImage: "",
				backImage: "",
				allImages: []
			};
			description = "";
		}

		if(dataJson.mainImage == null) dataJson.mainImage = "";


		/*var baseImgUrl = "http://www.edited.co.uk/images/product/" + newSlug.substr(0,1) + "/" + newSlug;
		var imgUrl = baseImgUrl + "-304px-304px.png";
		var largeImgUrl = baseImgUrl + "-600px-600px.png";
		var backImageUrl = baseImgUrl + "-add-1-304px-304px.png";*/

		var order_only = false;

		if(prod.collection) {
			order_only = (prod.collection.data.slug.indexOf("order-only") !== -1);
		}

		var sale_item = false;
		var sale_percentage = 0;
		var original_price = 0;

		if(prod.collection) {
			sale_item = (prod.collection.data.slug.indexOf("sale-item") !== -1);
			if(sale_item) {
				sale_percentage = ((1 - (prod.price.data.raw.with_tax / prod.sale_price)) * 100).toFixed(0);
				original_price = '£' + prod.sale_price.toFixed(2);
			}
		}

		//Get hold of furniture config data
		var furni_modifiers = {};

		if(typeof(prod.modifiers) == "object") {

			//$scope.furni_configurable = true;


			for(var key in prod.modifiers) {

				var modifier = prod.modifiers[key];

				furni_modifiers[modifier.title] = {
					modifierId: modifier.id,
					colourSets: {}
				};

				for(var key2 in modifier.variations) {

					var variant = modifier.variations[key2];

					var colourSet = variant.title.split('%')[0];

					var colourName = variant.title.split('%')[1];

					if(furni_modifiers[modifier.title].colourSets[colourSet] == undefined) {
						furni_modifiers[modifier.title].colourSets[colourSet] = [];
					}

					furni_modifiers[modifier.title].colourSets[colourSet].push({
						colourId: variant.id,
						colourName: colourName,
						price: parseInt(variant.mod_price.match(/[0-9]+/)[0])
					});

				}

			}

		}

		var category = prod.category.value;
		var category_link = prod.category.data[Object.keys(prod.category.data)[0]].slug;

		var stock_amount_text;

		if(prod.stock_level == 0) {
			stock_amount_text = "";
		} else if(prod.stock_level == 1) {
			stock_amount_text = "Last one!";
		} else if(prod.stock_level <= 3) {
			stock_amount_text = "Only " + prod.stock_level + " remaining!";
		} else if(prod.stock_level <= 10) {
			stock_amount_text = prod.stock_level + " available";
		} else {
			stock_amount_text = "More than 10 available";
		}

		return {
			id: prod.id,
			title: prod.title,
			slug: newSlug,
			in_stock: (prod.stock_level > 0),
			in_stock_text: (prod.stock_level > 0) ? "IN STOCK" : "OUT OF STOCK",
			stock_amount_text: stock_amount_text,
			stock_level: prod.stock_level,
			description: description,
			price: prod.price.value,
			rawPrice: prod.price.data.raw.with_tax,
			originalPrice: prod.price.data.raw.with_tax,
			image: dataJson.mainImage,
			largeImage: dataJson.mainImage,
			backImage: dataJson.backImage,
			allImages: dataJson.allImages,
			brand: prod.brand.value,
			order_only: order_only,
			sale_item: sale_item,
			sale_percentage: sale_percentage,
			original_price: original_price,
			furni_modifiers: furni_modifiers,
			category: category,
			category_link: category_link,
			gtin: dataJson.gtin,
			googleId: dataJson.googleId
		};

	}

	//----Display product grid----//
	function productsGridView(productsForGrid, alreadyLoaded, loadingNextSet) {

		if($scope.viewCategory !== undefined) {
			$scope.setMetaTags({
				title: $scope.viewCategory.title + " | Edited",
				description: $scope.viewCategory.title + " from Edited"
			});
			if(!loadingNextSet) $scope.updateNavigationHistory($scope.viewCategory.slug);
		} else {

			if($scope.filterType == "sale") {
				$scope.setMetaTags({
					title: "Sale | Edited",
					description: "Displaying all Edited sale items"
				});

				eventTracking.navigateToSale();

				if(!loadingNextSet) $scope.updateNavigationHistory("sale");

			} else {
				$scope.setMetaTags({
					title: "Filtered Products | Edited",
					description: "Displaying filtered products from Edited"
				});
			}
		}



		$scope.productGridLoaded = true;

		//if($scope.viewCategory != undefined) eventTracking.navigateToCategory($scope.viewCategory.title);

		if(!loadingNextSet) $scope.displayProducts = [];

		/*function translateProductJsonArray(products) {
			var newArr = [];

			for(var i = 0; i < products.length; i ++) {
				var prod = products[i];

				var imgUrl = "http://www.edited.co.uk/images/product/" + prod.slug.substr(0,1) + "/" + prod.slug + "-304px-304px.png";

				var newProd = {
					title: prod.title,
					slug: prod.slug,
					in_stock: (prod.stock_level > 0),
					stock_level: prod.stock_level,
					description: prod.description,
					price: prod.price.value,
					image: imgUrl
				}

				newArr.push(newProd);
			}

			return newArr;
		}*/

		function handleLazyRender() {
	    	$(".prodGrid-finalImage").one('load', function(e) {
	            $(e.currentTarget).parent().find(".prodGrid-loaderImage").hide();
	            $(e.currentTarget).fadeIn(500);
	        }).each(function() {
	          if(this.complete) $(this).load();
	        });
	    }

		var newArr = [];

	    for(var i = 0; i < productsForGrid.length; i ++) {
	    	if(productsForGrid[i].isGroupedProduct) {
	    		newArr.push(productsForGrid[i]);
	    	} else {
	    		newArr.push(translateProductToReadableJson(productsForGrid[i]));
	    	}
	    }

	    if(loadingNextSet) $scope.displayProducts = $scope.displayProducts.concat(newArr);
	    else $scope.displayProducts = newArr;
		
		//if(!alreadyLoaded) $scope.$apply();
		$scope.applyAngularScope();
		

		//Fix using timeout here, wait for dom render rather than ambiguous timings
		//window.setTimeout(handleLazyRender, 1000);
		finishLoading();

		$scope.loadNextSetInProgress = false;

	}


	//----Display product details----//
	function productDetailsView(product, alreadyLoaded) {

		if(product == undefined) {
			//Product not loaded so take you back home please
			$scope.redirectInErrorToHome();
			return;
		} else if(product.isGroupedProduct) {

			$scope.product = product;
			window.productImageCount = $scope.product.allImages.length;

			$scope.variants = product.variants;
			$scope.variantArray = product.variantArray;
			$scope.productHasVariants = true;
			$scope.parentProduct = product;

			$scope.displayProductCategoryLink = false;
		} else {
			$scope.product = translateProductToReadableJson(product);

			window.productImageCount = $scope.product.allImages.length;

			//All options selected, product is good to use
			$scope.productReadyForCart = true;

			//Find out if the product has parent with variants and if this is a child variant
			for(var key in $scope.groupedProducts) {
				var curGroup = $scope.groupedProducts[key];
				if(compareSlugs(key, sanitiseSlug(product.slug)) >= curGroup.matchLength) {
					$scope.parentProduct = curGroup;
					$scope.variants = curGroup.variants;
					$scope.variantArray = curGroup.variantArray;
					$scope.productHasVariants = true;

					//Preselect variants if possible
					var variantIterator;
					for(var i = 0; i < curGroup.variantArray.length; i ++) {

						var checkSlug = curGroup.variantArray[i].slug;

						if(checkSlug == undefined) checkSlug = curGroup.variantArray[i].comboName.toLowerCase();

						if(productSlug.indexOf("-" + checkSlug) != -1) {
							variantIterator = curGroup.variantArray[i];
							$scope.selectedVariant1 = variantIterator;
							$scope.subVariants1 = variantIterator.subVariants;
						}
					}

					var subVariantIterator = variantIterator.subVariants;
					if(subVariantIterator != undefined) {
						for(var i = 0; i < subVariantIterator.length; i ++) {

							var checkSlug = subVariantIterator[i].slug;
							if(checkSlug == undefined) checkSlug = subVariantIterator[i].comboName.toLowerCase(); 

							if(productSlug.indexOf("-" + checkSlug) != -1) {
								$scope.selectedVariant2 = subVariantIterator[i];
							}
						}
					}

				}

				//Calculate link for display here
				$scope.displayProductCategoryLink = true;

			}


		}

		//Find out of product is a furniture with modifiers
		/*if($scope.furni_configurable) {
			if(localStorage["furniture_config"] !== undefined) {
				var localFurniConfig = JSON.parse(localStorage["furniture_config"]);
				$scope.furni_operations = localFurniConfig[$scope.product.slug];

				for(var key in $scope.furni_operations) {
					$scope.product.rawPrice = $scope.product.rawPrice + $scope.furni_operations[key].priceIncrease;
					$scope.product.price = "£" + ($scope.product.rawPrice).toFixed(2);
				}

				var complete = true;
				for(var key in $scope.furni_operations) {
					if($scope.furni_operations[key] == false) complete = false;
					else $scope.furni_selectionMade = true;
				}

				$scope.furni_selectionsFinished = complete;

			} else {
				for(var i = 0; i < $scope.furni_config.length; i ++) {
					$scope.furni_operations[$scope.furni_config[i].modifier] = {
						mode: "selectSet",
						selectedColour: "",
						selectedColourGroup: "",
						colourGroup: "",
						priceIncrease: 0,
						availableColours: [],
						selectionComplete: false
					};
				}
			}


			//console.log($scope.furni_operations);
		}*/


		var metaDescr = "Designed by " + $scope.product.brand + " | " + $scope.product.description;

		if(product.description.length > 150) {
			metaDescr = metaDescr + "...";
		} 

		$scope.setMetaTags({
			title: $scope.product.title + " | Edited",
			description: metaDescr
		});

		$scope.structuredProductJson = JSON.stringify({
			"@context": "http://schema.org/",
			"@type": "Product",
			"name": $scope.product.title,
			"image": $scope.product.allImages[0],
			"description": $scope.product.description,
			"brand": {
				"name": $scope.product.brand
			},
			"offers": {
				"@type": "Offer",
				"priceCurrency": "GBP",
				"price": $scope.product.price
			}
		});

		//Need to go off an preselect the variants here


		//Find out if you need
		
		$scope.displayProductDetails = true;


		eventTracking.viewProduct($scope.product.title, $scope.product.googleId);

		finishLoading();

		$scope.applyAngularScope();

		$timeout(activateLightslider, 1000);

		$scope.loadRelatedProducts();
		
	}

	function activateLightslider() {

		var loadCount = 0;

		$(".finalImage").load(function() {
			loadCount++;
			//alert("loaded");
			if(loadCount == window.productImageCount) {

				$('#lightSlider').lightSlider({
				    gallery: true,
				    item: 1,
				    loop:true,
				    slideMargin: 0,
				    thumbItem: 9,
				    speed: 400, //ms'
			        auto: true,
			        loop: true,
			        cssEasing: 'ease',
			        slideEndAnimation: true,
			        pause: 5000,
			        onSliderLoad: function() {
			        	window.setTimeout(function() {
							$(".finalImage").unbind();
							$(".loaderImage").hide();
							$(".finalImage").css("display", "block");
							$(".lightsliderImages").css("opacity",1);

			        	}, 1500);

						$(".lSGallery").css("transition", "max-height 0.2s");
		        		$(".lSGallery").css("max-height", "50px");
		        		/*$("#lightSlider").css("max-width", "none");*/
			        }
				});


			}
		});

	}

	$(function() {

		//$(".cartAnimObj").css("background", "transparent");
		
		//Move the cart object if the window is resized
		$( window ).resize(function() {
			//$scope.setCartAnimPos();
		});

		//$scope.setCartAnimPos();

	});

	//User has asked for a reminder to be sent for this product
	$scope.submitReminder = function() {

		$scope.reminder_error = true;

		/*$scope.reminder_submitted = true;
		$scope.reminder_signupNewsLetter = true;
		$scope.reminder_email = "";
		$scope.reminder_error = false;*/
	}

}])
app.controller('create_account_controller', ['$scope', 'remoteRequestService', function($scope, remoteRequestService) {
    
    console.log("--Account Controller--");

    $scope.signupNewsletter = true;

    $scope.createAccount = function(userJson) {

    	$scope.createInProgress = true;
    	$scope.creationError = false;
    	$scope.fatalError = false;
        $(".account-submitButton").prop("disabled",true);


        function createAccountSuccess(response) {
            var data = response.data;
            $(".account-submitButton").prop("disabled",false)
            $scope.createInProgress = false;

            if(data.status) $scope.accountCreatedSuccessfully = true;
            else {
                $scope.creationError = true;

                var error = data.errors[0];

                if(error == "Email Address must be unique") error = "An account already exists with this email address";

                $scope.errorInfo = error;
            }
        }

        function createAccountFailure(response) {
            $scope.createInProgress = false;
            $scope.fatalError = true;
            $(".account-submitButton").prop("disabled",false);
        }

        userJson["signupNewsletter"] = $scope.signupNewsletter;

        remoteRequestService.createAccountCall(userJson, createAccountSuccess, createAccountFailure);

    	

    }

}])
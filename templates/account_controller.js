app.controller('account_controller', ['$scope', 'remoteRequestService', '$cookies', '$location', 'eventTracking', '$timeout', function($scope, remoteRequestService, $cookies, $location, eventTracking, $timeout) {
    
    $scope.rememberMe = true;

    $scope.passwordResetMode = false;

    $scope.loginInProgress = false;

    $scope.passwordResetInProgress = false;

    $scope.accountCreationInProgress = false;

    $scope.createAccountMode = false;

    $scope.signupNewsletter = true;

    //Loading data
    $scope.loadingOrders = true;
    $scope.errorLoadingOrders = false;
    $scope.customerOrders;
    $scope.orderIssue = true;

    $scope.loadingAddresses = true;
    $scope.errorLoadingAddresses = false;
    $scope.customerAddresses;
    $scope.deletingAddress = false;
    $scope.errorDeletingAddress = false;

    $scope.showPasswordChange = false;

    $scope.passMatch = true;
    $scope.resetInProgress = false;
    $scope.passwordResetSuccessfully = false;
    $scope.resetError = false;
    $scope.resetErrorInfo;
    $scope.fatalError = false;

    $scope.newPass;
    $scope.newPassRepeat;

    $scope.passForm = {};



    $scope.createAccount = function(userJson) {

        $scope.accountCreationInProgress = true;
        $scope.creationError = false;
        $scope.creationFatalError = false;

        eventTracking.createAccount();

        function createAccountSuccess(response) {
            var data = response.data;

            $scope.accountCreationInProgress = false;

            if(data.status) {
                $scope.accountCreatedSuccessfully = true;
                $scope.createAccountMode = false;
            } else {
                $scope.creationError = true;

                var error = data.errors[0];

                if(error == "Email Address must be unique") error = "An account already exists with this email address";

                $scope.creationErrorInfo = error;
            }
        }

        function createAccountFailure(response) {
            $scope.accountCreationInProgress = false;
            
            $scope.creationFatalError = true;
        }

        userJson["signupNewsletter"] = $scope.signupNewsletter;

        remoteRequestService.createAccountCall(userJson, createAccountSuccess, createAccountFailure);

    }

    $scope.login = function(userJson) {
        if($scope.loginInProgress) return;

        $scope.loginInProgress = true;
        $scope.loginError = false;
        $scope.fatalError = false;

    	$(".account-loginButton").prop("disabled",true);

        eventTracking.login();

        function loginCustomerSuccess(response) {
            var data = response.data;
            $(".account-loginButton").prop("disabled",false);
            $scope.loginInProgress = false;

            if(data.status) {
                $scope.loginSuccessful = true;
                $scope.loginUser(data.result);
                if($scope.rememberMe) saveLoginCookie(data.result);

                $scope.loadCustomerData();

            } else {
                $scope.loginError = true;
                $scope.errorInfo = data.errors.join("<br>");
            }
        }

        function loginCustomerFailure(response) {

    		$scope.fatalError = true;
    		$scope.loginInProgress = false;
    		$(".account-loginButton").prop("disabled",false);

        }

        remoteRequestService.loginCustomerCall(userJson, loginCustomerSuccess, loginCustomerFailure);

    }

    $scope.resetPassword = function() {

        if($scope.passwordResetInProgress) return;

        $scope.passwordResetInProgress = true;
        $scope.resetError = false;
        $scope.fatalError = false;

        var email = this.account.email;

        eventTracking.resetPassword();

        function resetPasswordSuccess(response) {

            var data = response.data;
            $scope.passwordResetInProgress = false;

            if(data.status) {
                $scope.resetSuccessful = true;
            } else {
                $scope.resetError = true;

                if(data.errors != undefined) {
                    $scope.errorInfo = data.errors.join("<br>");
                }
            }
        }

        function resetPasswordFailure(response) {

            $scope.fatalError = true;
            $scope.passwordResetInProgress = false;

        }

        remoteRequestService.resetCustomerPasswordCall(email, resetPasswordSuccess, resetPasswordFailure);

    }

    $scope.changeAccountPass = function(newPass) {

        function resetPasswordSuccess(response) {
            var data = response.data;
            //$(".account-resetButton").prop("disabled",false)
            $scope.resetInProgress = false;


            if(data.status) {
                $scope.passwordResetSuccessfully = true;
                
            } else {
                $scope.resetError = true;
                $scope.resetErrorInfo = data.errors[0];
            }

            $scope.showPasswordChange = false;

            $scope.newPass = "";
            $scope.newPassRepeat = "";

            $timeout(function() {
                $scope.applyAngularScope();
            }, 200);
        }

        function resetPasswordFailure(response) {
            $scope.resetInProgress = false;
            $scope.fatalError = true;
            //$(".account-resetButton").prop("disabled",false);
        }

        if($scope.passForm.newPass !== $scope.passForm.newPassRepeat) {
            $scope.passMatch = false;
        } else if(!$scope.resetInProgress) {
            $scope.passMatch = true;
            $scope.resetInProgress = true;

            //$(".account-resetButton").prop("disabled",true);

            eventTracking.changeCustomerPassword();

            remoteRequestService.changeCustomerPassword($scope.loggedInCustomerJson.id, $scope.loggedInCustomerJson.email, $scope.passForm.newPass, resetPasswordSuccess, resetPasswordFailure);
        }


    }



    function saveLoginCookie(userJson) {

        var expiryDate = new Date();
        //7 day expiry
        expiryDate.setDate(expiryDate.getDate() + 7);

        //Now we need this token
        //delete userJson.token;

        var cookieDefaults = {
            domain: "editedfresh.co.uk",
            expires: expiryDate
        };

        //$cookies.putObject("editedWebstoreCookieCache", userJson, cookieDefaults);

        $cookies.putObject("editedWebstoreCookieCache", userJson);


    }

    $scope.loadCustomerData = function(addressOnly) {

        if(!this.loggedIn) return;

        function orderSuccess(data) {
            $scope.loadingOrders = false;
            $scope.customerOrders = data.data;
        }

        function orderFailure() {
            $scope.loadingOrders = false;
            $scope.errorLoadingOrders = true;
        }

        function addressSuccess(data) {
            $scope.loadingAddresses = false;
            $scope.loggedInAddressesLoaded = true;
            $scope.loggedInCustomerAddresses = data.data.result;
        }

        function addressFailure() {
            $scope.loadingAddresses = false;
            $scope.errorLoadingAddresses = true;

        }

        var customerId = this.loggedInCustomerJson.id,
        customerToken = this.loggedInCustomerJson.token;

        if(!addressOnly) {
            remoteRequestService.getFilteredOrders(customerId, orderSuccess, orderFailure);
        }

        remoteRequestService.getAddressesForCustomer(customerId, customerToken, addressSuccess, addressFailure);


    }

    $scope.loadCustomerData();

    $scope.goToReceipt = function(orderId) {

        $location.path("/receipt" + orderId);

        eventTracking.viewPreviousReceipt();

    }

    $scope.formatAccountDate = function(date) {
        return new Date(date);
    }

    $scope.formatAddressForGrid = function(address) {

        var addressString = address.address_1 + ",  ";
        if(address.address_2 != "") addressString += address.address_2 + ",  ";
        addressString += address.city + ",  " + address.county + ",  " + address.postcode + ",  " + address.country.value;

        return addressString;

    }

    $scope.deleteAddress = function(addressId) {

        if($scope.deletingAddress) return;

        $scope.deletingAddress = true;
        $scope.errorDeletingAddress = false;

        $scope.completeDelete = function(addressId) {
            

            eventTracking.deleteAddress();

            var customerId = this.loggedInCustomerJson.id;

            function deleteSuccess() {
                $scope.deletingAddress = false;
                $scope.loadCustomerData(true);
            }

            function deleteFailure() {
                $scope.deletingAddress = false;
                $scope.errorDeletingAddress = true;
            }

            remoteRequestService.deleteAddressForCustomer(customerId, addressId, deleteSuccess, deleteFailure);

        }

        vex.dialog.confirm({
            message: 'Are you sure you want to delete this saved address?',
            callback: function(value) {
                if(value) {
                    $scope.completeDelete(addressId);
                } else {
                    $scope.deletingAddress = false;
                    $scope.errorDeletingAddress = false;
                    $scope.applyAngularScope();
                }
            }
        });
    }



}])
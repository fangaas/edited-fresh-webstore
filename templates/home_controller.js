app.controller('home_controller', ['$scope', function($scope) {
    
    console.log("--Home Controller--");


    /*---Handle colour overlays---*/
    var colours = ['it-hover-teal', 'it-hover-orange', 'it-hover-pink', 'it-hover-yellow', 'it-hover-navy', 'it-hover-rose', 'it-hover-red', 'it-hover-green'];

    $(".homeGrid-item").hover(function() {

        $(this).find(".homeGrid-colorOverlay").addClass(colours[Math.floor(Math.random()*colours.length)] + " homeGrid-colorOverlayFullOpacity");

    }, function() {

        $(this).find(".homeGrid-colorOverlay").removeClass(colours.join(" ") + " homeGrid-colorOverlayFullOpacity");

    });

    var feed = new Instafeed({
        get: 'user',
        clientId: "1afc3b4795fa432898d3af6bf592abe3",
        accessToken: '303916489.1677ed0.017d75b3adeb4ffc916adbf40391a38d',
        userId: 303916489,
        sortBy: 'most-recent',
        links: true,
        limit: '1',
        resolution: 'standard_resolution',
        template: '<a href="{{link}}"><img src="{{image}}" class=""/><div class = "homeGrid-colorOverlay"></div><div class="homeGrid-itemText"><div class = "homeGrid-textAligner"><h3><i class="fa fa-instagram fa-3x"></i></h3><h5>Latest on Instagram.</h5></div></div></a>'
    });
    feed.run();    

    /*---Handling lazy image loading for home---*/
    $(function() {

        window.setTimeout(function() {
            $(".homeGrid-finalImage").one('load', function(e) {
                $(e.currentTarget).parent().find(".homeGrid-loaderImage").hide();
                $(e.currentTarget).css('display', 'block').hide().fadeIn(500);
            }).each(function() {
              if(this.complete) $(this).load();
            });
        }, 100);


        $("body").animate({scrollTop:0});
        $('.sidebar-menu ul').slideUp(300);


    });

    $scope.setCatForSidebar("");

}])
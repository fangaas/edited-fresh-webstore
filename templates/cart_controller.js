app.controller('cart_controller', ['$scope', '$routeParams', 'moltinApiService', '$location', 'eventTracking', function($scope, $routeParams, moltinApiService, $location, eventTracking) {

	$scope.navigateToCheckout = function() {
		eventTracking.goToCheckout();

		$location.path('/checkout');
	}

	$scope.checkoutCartMode = false;

	
	//Go through the cart and calculate data for remarketing
	var productIdArray = [],
	totalValue = 0,
	returnCustomer = $scope.loggedIn;

	if($scope.userCart !== null) {

		var contents = $scope.userCart.contents;

		for(var key in contents) {

			var itemJson = JSON.parse(contents[key].description.split("%endjson")[0]);

			productIdArray.push(itemJson.googleId);

		}

		if($scope.userCart.totals !== undefined) totalValue = $scope.userCart.totals.post_discount.raw.with_tax;

		if(productIdArray.length > 0) {

			eventTracking.remarketCart(productIdArray, totalValue, returnCustomer);

		}

	}


}]);
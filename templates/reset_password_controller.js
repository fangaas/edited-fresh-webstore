app.controller('reset_password_controller', ['$scope', 'moltinApiService', 'remoteRequestService', '$location' , function($scope, moltinApiService, remoteRequestService, $location) {

	if(!$scope.loggedIn) {
		$location.path('/create_account');
	}

	$scope.newPass;
	$scope.newPassRepeat;

	$scope.passMatch = true;
	$scope.resetInProgress = false;
	$scope.passwordResetSuccessfully = false;
	$scope.resetError = false;
	$scope.resetErrorInfo;
	$scope.fatalError = false;


	$scope.resetPassword = function(newPass) {

		function resetPasswordSuccess(response) {
            var data = response.data;
            $(".account-resetButton").prop("disabled",false)
            $scope.resetInProgress = false;

            if(data.status) $scope.passwordResetSuccessfully = true;
            else {
                $scope.resetError = true;
                $scope.resetErrorInfo = data.errors[0];
            }
        }

        function resetPasswordFailure(response) {
            $scope.resetInProgress = false;
            $scope.fatalError = true;
            $(".account-resetButton").prop("disabled",false);
        }

        if($scope.newPass !== $scope.newPassRepeat) {
        	$scope.passMatch = false;
        } else if(!$scope.resetInProgress) {
        	$scope.passMatch = true;
        	$scope.resetInProgress = true;

            $(".account-resetButton").prop("disabled",true);

			remoteRequestService.changeCustomerPassword($scope.loggedInCustomerJson.id, $scope.loggedInCustomerJson.email, $scope.newPass, resetPasswordSuccess, resetPasswordFailure);
        }


	}

}]);

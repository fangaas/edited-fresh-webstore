app.service('eventTracking', function() {

    /*ga('send', {
            hitType: 'event',
            eventCategory: 'purchases',
            eventAction: 'add_to_cart',
            eventLabel: 'cats.mp4'
        });*/

    var fireRemarketingEvent = function(parameters) {

        window.google_trackConversion({
            google_conversion_id: 941419655, 
            google_custom_params: parameters,
            google_remarketing_only: true
        });

    }

    this.addToCart = function(productName) {

        ga('send', {
            hitType: 'event',
            eventCategory: 'cart_management',
            eventAction: 'add_to_cart',
            eventLabel: productName
        });

    }

    this.removeFromCart = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'cart_management',
            eventAction: 'remove_from_cart'
        });

    }

    this.clearCart = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'cart_management',
            eventAction: 'clear_cart'
        });

    }

    this.remarketCart = function(productIdArray, totalValue, returnCustomer) {

        fireRemarketingEvent({ 
            ecomm_pagetype: 'cart',
            ecomm_prodid: productIdArray,
            ecomm_totalvalue: totalValue,
            returnCustomer: returnCustomer
        });

    }

    this.goToStaticPage = function(page) {

        ga('send', {
            hitType: 'event',
            eventCategory: 'browsing',
            eventAction: 'visit_static_page',
            eventLabel: page
        });

        fireRemarketingEvent({
            ecomm_pagetype: 'other'
        });

    }

    this.runSearch = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'browsing',
            eventAction: 'perform_search'
        });

        fireRemarketingEvent({
            ecomm_pagetype: 'searchresults'
        });

    }

    this.showFeedbackPopup = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'browsing',
            eventAction: 'show_feedback_popup'
        });

    }

    this.goStraightToCart = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'browsing',
            eventAction: 'go_to_cart_after_adding'
        });

    }    

    this.navigateToCategory = function(category) {

        ga('send', {
            hitType: 'event',
            eventCategory: 'browsing',
            eventAction: 'navigate_to_category',
            eventLabel: category
        });

        fireRemarketingEvent({
            ecomm_pagetype: 'category',
            ecomm_category: category
        });

    }

    this.navigateToSale = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'browsing',
            eventAction: 'view_sale'
        });

    }

    this.viewProduct = function(productName, productGoogleId) {

        ga('send', {
            hitType: 'event',
            eventCategory: 'browsing',
            eventAction: 'view_product',
            eventLabel: productName
        });

        fireRemarketingEvent({
            ecomm_pagetype: 'product',
            ecomm_prodid: productGoogleId
        });

    }

    this.createAccount = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'account_management',
            eventAction: 'create_account'
        });        

    }

    this.login = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'account_management',
            eventAction: 'login'
        });

    }

    this.logout = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'account_management',
            eventAction: 'logout'
        });        

    }

    this.resetPassword = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'account_management',
            eventAction: 'reset_password'
        });

    }

    this.changeCustomerPassword = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'account_management',
            eventAction: 'customer_changed_password'
        });

    }

    this.viewPreviousReceipt = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'account_management',
            eventAction: 'viewing_previous_receipt'
        });

    }

    this.deleteAddress = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'account_management',
            eventAction: 'delete_address'
        });

    }

    this.goToCheckout = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'checkout_process',
            eventAction: 'go_to_checkout'
        });

    }

    this.navigateCheckout = function(checkoutSection) {

        ga('send', {
            hitType: 'event',
            eventCategory: 'checkout_process',
            eventAction: 'navigate_checkout',
            eventLabel: checkoutSection
        });

    }


    this.choosePaymentMethod = function(method) {

        ga('send', {
            hitType: 'event',
            eventCategory: 'checkout_process',
            eventAction: 'choose_payment_method',
            eventLabel: method
        });

    }

    this.selectSavedAddress = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'checkout_process',
            eventAction: 'use_saved_address'
        });

    }

    this.completeOrder = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'checkout_process',
            eventAction: 'complete_order'
        });

    }

    /*this.successfulPayment = function(conversionAmount) {

        //Track conversion with FB
        try {
            fbq('track', 'Purchase', {
                value: cartAmount,
                currency: 'GBP'
            });
        } catch(e) {}

        //Track conversion with google
        try {
            ga('send', {
                hitType: 'event',
                eventCategory: 'checkout_process',
                eventAction: 'conversion',
                eventValue: conversionAmount
            });
        } catch(e) {}

        //Track conversion with Bing
        try {
            window.uetq = window.uetq || [];
            window.uetq.push({ 'ec':'checkout_process', 'ea':'conversion', 'el':'', 'ev': conversionAmount });
        } catch(e) {}

    }*/

    this.directToReceipt = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'checkout_process',
            eventAction: 'direct_to_receipt'
        });

    }

    this.goBackFromConfirm = function() {

        ga('send', {
            hitType: 'event',
            eventCategory: 'checkout_process',
            eventAction: 'go_back_from_confirmation'
        });

    }






});
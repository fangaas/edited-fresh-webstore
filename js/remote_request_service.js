app.service('remoteRequestService', ['$http', function($http) {

	console.log("request service init");

	var baseUrl = "https://edited.co.uk:4830";

	function buildUrl(extension) {
		return baseUrl + "/" + extension;
	}

	this.createAccountCall = function(userJson, successCallback, failureCallback) {

		$http.post(buildUrl("addCustomer"), userJson).then(successCallback, failureCallback);

	}

	this.loginCustomerCall = function(userJson, successCallback, failureCallback) {

		$http.post(buildUrl("loginCustomer"), userJson).then(successCallback, failureCallback);

	}

    this.logoutCustomerCall = function(userJson, successCallback, failureCallback) {

        $http.post(buildUrl("logoutCustomer"), userJson).then(successCallback, failureCallback);

    }

    this.resetCustomerPasswordCall = function(email, successCallback, failureCallback) {

        var userJson = {
            email: email
        };

        $http.post(buildUrl("resetCustomerPassword"), userJson).then(successCallback, failureCallback);

    }

    this.stripeValidateCard = function(cardData, successCallback, failureCallback) {

        var userJson = {
            cardData: cardData
        };

        $http.post(buildUrl("authoriseCard"), userJson).then(successCallback, failureCallback);

    }

	this.payForOrderOnsite = function(orderId, cardData, accountCreationData, successCallback, failureCallback) {

		var userJson = {
			orderId: orderId,
			cardData: cardData,
            accountCreationData: accountCreationData
		};

		$http.post(buildUrl("completeOrderOnsite"), userJson).then(successCallback, failureCallback);

    }

    this.paypalPayForOrder = function(amount, paypalToken, paypalPayerId, orderId, accountCreationData, successCallback, failureCallback) {

        var userJson = {
            amount: amount,
            paypalToken: paypalToken,
            paypalPayerId: paypalPayerId,
            orderId: orderId,
            accountCreationData: accountCreationData        
        };

        $http.post(buildUrl("completeOrderPaypal"), userJson).then(successCallback, failureCallback);

    }

    /*this.paypalCompleteOrder = function(orderId, accountCreationData, successCallback, failureCallback) {

        var userJson = {
            orderId: orderId,
            accountCreationData, accountCreationData
        };

        $http.post(buildUrl("completeOrderPaypal"), userJson).then(successCallback, failureCallback);

    }*/

    this.getOrderSummary = function(orderId, successCallback, failureCallback) {

    	var orderJson = {
    		orderId: orderId
    	};

    	$http.post(buildUrl("getOrderSummary"), orderJson).then(successCallback, failureCallback);

    }

    this.getOrderItems = function(orderId, successCallback, failureCallback) {

    	var orderJson = {
    		orderId: orderId
    	};

    	$http.post(buildUrl("getOrderItems"), orderJson).then(successCallback, failureCallback);

    }

    this.getFilteredOrders = function(customerId, successCallback, failureCallback) {

        var filterJson = {
            customer: customerId
        };

        $http.post(buildUrl("getFilteredOrders"), filterJson).then(successCallback, failureCallback);

    }

    this.findCustomerByEmail = function(emailAddress, successCallback, failureCallback) {

    	var findJson = {
    		email: emailAddress
    	};

    	$http.post(buildUrl("findCustomerByField"), findJson).then(successCallback, failureCallback);

    }

    this.getAddressesForCustomer = function(customerId, customerToken, successCallback, failureCallback) {

    	var customerJson = {
    		id: customerId,
            token: customerToken
    	};

    	$http.post(buildUrl("getAddressesForCustomer"), customerJson).then(successCallback, failureCallback);

    }

    this.deleteAddressForCustomer = function(customerId, addressId, successCallback, failureCallback) {

        var addressJson = {
            addressId: addressId,
            customerId: customerId
        };

        $http.post(buildUrl("deleteAddressForCustomer"), addressJson).then(successCallback, failureCallback);

    }

    this.changeCustomerPassword = function(customerId, email, newPass, successCallback, failureCallback) {

        var customerJson = {
            id: customerId,
            email: email,
            pass: newPass
        };

        $http.post(buildUrl("changeCustomerPassword"), customerJson).then(successCallback, failureCallback);        

    }

    this.paypalSetExpressCheckout = function(amount, orderId, accountCreationData, successCallback, failureCallback) {

        var paypalJson = {
            amount: amount,
            orderId: orderId,
            accountCreationData: accountCreationData            
        };

        $http.post(buildUrl("paypalSetExpressCheckout"), paypalJson).then(successCallback, failureCallback);  

    }

    this.sendContactFormEmail = function(json, successCallback, failureCallback) {

        var contactJson = json;

        $http.post(buildUrl("sendContactFormEmail"), contactJson).then(successCallback, failureCallback);

    }



}]);

var app = angular.module('app', ['ngRoute', 'ngSanitize', 'ngCookies', 'ngMessages', 'ngMeta', 'updateMeta', 'ngResource'])
//ngAnimate

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

app.config(['$routeProvider', '$locationProvider', 'ngMetaProvider', function($routeProvider, $locationProvider, ngMetaProvider) {
	$routeProvider
	    .when('/', {
			templateUrl: 'templates/home.html',
			controller: 'home_controller',
			meta: {
				'title': "Edited: Eclectic Interiors and Homewares",
				'description': "Offering a curated collection of home furnishings, lighting, artwork, cookware and soft furnishings specialising in design-led interiors",
				'robots': 'index,follow' 
			}
	    })
	    .when('/account', {
	    	templateUrl: 'templates/account.html',
	    	controller: "account_controller",
	    	meta: {
	    		'title': 'Account | Edited',
				'robots': 'noindex'
			}
	    })
	    .when('/create_account', {
	    	templateUrl: 'templates/create_account.html',
	    	controller: "create_account_controller",
	    	meta: {
	    		'title': 'Create New Account | Edited',
				'robots': 'noindex'
			}
	    })
	    .when('/reset_password', {
	    	templateUrl: 'templates/reset_password.html',
	    	controller: 'reset_password_controller',
	    	meta: {
	    		'title': 'Reset Password | Edited',
				'robots': 'noindex'
			}
	    })
	    .when('/cart', {
	    	templateUrl: 'templates/cart.html',
	    	controller: 'cart_controller',
	    	meta: {
	    		'title': 'Cart | Edited',
				'robots': 'noindex'
			}
	    })
	    .when('/checkout', {
    		templateUrl: 'templates/checkout.html',
    		controller: 'checkout_controller',
    		meta: {
    			'title': 'Checkout | Edited',
				'robots': 'noindex'
			}
	    })
	    .when('/search', {
	    	templateUrl: 'templates/products.html',
	    	controller: 'search_controller',
	    	meta: {
	    		'title': 'Search | Edited',
				'robots': 'noindex'
			}
	    })
	    .when('/brand', {
	    	templateUrl: 'templates/brand.html',
	    	controller: 'brand_controller',
	    	meta: {
				'title': "Brands | Edited",
				'description': "View our products filtered by our most popular brands, such as HAY",
				'robots': 'index,follow' 
			}
	    })
	    .when('/our-stores', {
	    	templateUrl: 'static_pages/our_stores.html',
	    	controller: 'static_page_controller',
	    	meta: {
				'title': "Our Store | Edited",
				'description': "Information about our brick and mortar store",
				'robots': 'index,follow' 
			}
	    })
	    .when('/about-us', {
	    	templateUrl: 'static_pages/about_us.html',
	    	controller: 'static_page_controller',
	    	meta: {
				'title': "About Us | Edited",
				'description': "Information about Edited",
				'robots': 'index,follow' 
			}
	    })
	    .when('/delivery-and-returns', {
	    	templateUrl: 'static_pages/delivery_and_returns.html',
	    	controller: 'static_page_controller',
	    	meta: {
				'title': "Delivery And Returns | Edited",
				'description': "Information relating to product delivery and returns",
				'robots': 'index,follow' 
			}
	    })
	    .when('/privacy-policy', {
	    	templateUrl: 'static_pages/privacy_policy.html',
	    	controller: 'static_page_controller',
	    	meta: {
				'title': "Privacy Policy | Edited",
				'description': "Privacy policy for Edited Online",
				'robots': 'index,follow' 
			}
	    })
	    .when('/terms-and-conditions', {
	    	templateUrl: 'static_pages/terms_and_conditions.html',
	    	controller: 'static_page_controller',
	    	meta: {
				'title': "Terms And Conditions | Edited",
				'description': "Terms and conditions for Edited",
				'robots': 'index,follow' 
			}
	    })
	    .when('/product-submissions', {
	    	templateUrl: 'static_pages/product_submissions.html',
	    	controller: 'static_page_controller',
	    	meta: {
				'title': "Product Submissions | Edited",
				'description': "Product submissions to Edited",
				'robots': 'index,follow' 
			}
	    })
	    .when('/receipt:orderId', {
	    	templateUrl: 'templates/receipt.html',
	    	controller: 'receipt_controller',
	    	meta: {
	    		'title': 'Order Receipt | Edited',
				'robots': 'noindex'
			}
	    })
	    .when('/:productSlug', {
	    	templateUrl: 'templates/products.html',
	    	controller: 'products_controller',
	    	meta: {
				'robots': 'index,follow'
			}
	    })
	    .otherwise('/');

    // use the HTML5 History API
   	$locationProvider.html5Mode(true);

}])

app.filter('unsafe', function($sce) { 
	return $sce.trustAsHtml; 
})

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
})

app.run(['$rootScope', '$location', 'ngMeta', function($rootScope, $location, ngMeta) {
	
	ngMeta.init();

	//On app run
	$rootScope.$watch(function() { 
		return $location.path();
    }, function(path){  

  		//Run each state change
      	//$rootScope.navCount++;

    });

    vex.defaultOptions.className = 'vex-theme-wireframe';

}])

/*app.service('MetaInformation', function() {
	var metaTitle = "Edited";
	return {
		metaTitle: function() { return metaTitle; }
	};
})*/

.controller('main_controller', ['$scope', '$rootScope', 'moltinApiService', '$sce', '$cookies', 'remoteRequestService', '$timeout', '$location', 'editedDataStore', 'eventTracking', 'ngMeta', function($scope, $rootScope, moltinApiService, $sce, $cookies, remoteRequestService, $timeout, $location, editedDataStore, eventTracking, ngMeta) {

	$scope.meta = {
		title: "Edited: Eclectic Interiors and Homewares",
		description: "Offering a curated collection of home furnishings, lighting, artwork, cookware and soft furnishings specialising in design-led interiors.",
		robotsNoFollow: false,
		robots: "index,follow,all",
		keywords: ""
	};

	$scope.globalDomain = "edited.co.uk";

	$scope.rawCartAmount = "£0.00";

	$scope.freeDeliveryText = "Spend just £30 and delivery is on us!";


	//Keep track of breadcrumb links
	$scope.breadcrumb = [{
		text: "HOME",
		href: "/"
	}, {
		text: "ACCOUNT",
		href: "/account"
	}];

	//JSON for all categories
	$scope.allCategories = editedDataStore.allCategories;

	//JSON for selected products
	$scope.homeProducts = editedDataStore.homeProducts;

	//JSON for all customer feedback
	$scope.allFeedback = editedDataStore.allFeedback;

	//JSON for all products
	$scope.allProducts = null;

	//JSON for all brands 
	$scope.allBrands = null;

	//JSON for all gateways (not loaded until going to checkout)
	$scope.allGateways = null;

	//JSON for all shipping methods (not loaded until going to checkout)
	$scope.allShippingMethods = null;

	//JSON for products that have been loaded already, save reloading them
	$scope.loadedProductsObject = {};
	//JSON for product groupings with variants
	$scope.groupedProductsObject = {};
	$scope.allProductsArray = [];

	//$rootScope.navCount = 0;
	$scope.searchQuery = "";
	$scope.minSearchQueryLength = 2;

	//If the cart adding is in progress
	$scope.cartAddingInProgress = false;

	$scope.maxAmountThatCanBeAddedToCart = 20;

	//IF its the first time products have been navigated to for display (in which case delay the lazy image loading)
	$scope.firstProductNav = true;

	//Syntax for intelligent product grouping
	$scope.groupedProducts = editedDataStore.groupedProducts;

	$scope.loggedIn = false;
	//Customer login json 
	$scope.loggedInCustomerJson = null;
	//Custom address array (loaded addresses used throughout)
	$scope.loggedInCustomerAddresses = [];
	$scope.loggedInAddressesLoaded = false;

	//JSON for users cart
	$scope.userCart = {
		total_items: 0
	};
	$scope.app_cartUpdating = false;
	$scope.app_cartEstimatedTotal = 0;

	$scope.freeDeliveryAmount = 30;

	//JSON for promotions
	$scope.promo_code = {
		active: false,
		code: "",
		discount: ""
	};

	//If we are going along to a category when navigating
	$scope.navigatingToCategory = false;

	//$scope.itemJustAdded = false;
	$scope.cat = "";

	$scope.checkoutCartMode = false;

	//If the user has qualified for free delivery due to over 50£ worth of purchase
	$scope.app_qualifiedForFreeDelivery = false;

	$scope.showDiscountPopup = false;

	//Variables for contact form
	$scope.contact_stage = "message";

	//Clear local storage for furniture configs (for now)
	localStorage.removeItem("furniture_config");

	$(function() {
		//moltinApiService.getCategoryTree(handleCategories);
		//Now loading in categories straight away
		handleCategories();

		//moltinApiService.getAllProducts(productsCallback);
		$scope.makeUserRequest({op: "retreiveCart"});

		window.setTimeout(function() {
			updateOpeningSign();
		}, 500);

		/*var slideout = new Slideout({
		    'panel': document.getElementById('core_container'),
		    'menu': document.getElementById('mobile_menu'),
		    'padding': 256,
		    'tolerance': 70
	  	});

	  	$(".mobile_menu_button").click(function() {
	  		slideout.toggle();
	  	});*/

		window.setTimeout(function() {
			$("#mobile_menu").mmenu({
				slidingSubmenus: true,
				navbar: {
					title: "Edited",
					titleLink: "none"
				},
				onClick: {
					close: true
				}
			});

			var mmenu = $("#mobile_menu").data( "mmenu" );

			$(".mobile_menu_button").click(function() {
				if($(".mm-opening .mobile_menu_button").length == 0) {
					mmenu.open();
				} else {
					mmenu.close();
				}
			});

			$("#mobile_menu").css("opacity", 1);
		}, 1000);

		//$('body').flowtype();
		window.clickBioep = function() {
			bioEp.hidePopup();
		}
		
		if(window.innerWidth <= 991 && $scope.showDiscountPopup) {
			//If using small screen likely tablet or mobile so wont see popup, show after delay
			bioEp.init({
				width: 700,
				height: 400,
				html: '<img onclick = "window.clickBioep()" src="images/before_leave.png" alt="Claim your discount!" />',
		        delay: 15,
		        showOnDelay: true,
		        cookieExp: 5
		    });
			
		} else if($scope.showDiscountPopup){
			bioEp.init({
				width: 700,
				height: 400,
				html: '<img onclick = "window.clickBioep()" src="images/before_leave.png" alt="Claim your discount!" />',
		        delay: 15,
		        showOnDelay: false,
		        cookieExp: 5
		    });
		}

	});

	$scope.navigationHistory = {
		lastGrid: "",
		scrollPercentage: 0,
		navigationDistance: 0
	};

	$rootScope.$on("$locationChangeStart", function(event, next, current) { 
		var path = current.split(/\/+/g).pop();

		if(path == $scope.navigationHistory.lastGrid.toLowerCase() || ($scope.navigationHistory.lastGrid == "sale" && path.endsWith("sale"))) {
			$scope.navigationHistory.scrollPercentage = $("body").scrollTop();
		} else {
			$scope.navigationHistory.navigationDistance++;
			if($scope.navigationHistory.navigationDistance > 2) {
				$scope.navigationHistory = {
					lastGrid: "",
					scrollPercentage: 0,
					navigationDistance: 0
				};				
			}
		}
	});

	$scope.updateNavigationHistory = function(gridName) {

		//data is the name of the current navigation frame
		if($scope.navigationHistory.lastGrid == gridName) {

			$timeout(function() {
				//Dont move the scroll position if the user has already re-scrolled to minimise confusion
				if($("body").scrollTop() == 0)  $("body").animate({scrollTop: $scope.navigationHistory.scrollPercentage++});
			}, 1000);

		} else {
			$scope.navigationHistory = {
				lastGrid: gridName,
				scrollPercentage: 0,
				navigationDistance: 0
			}
		}

	}

	$scope.setMetaTags = function(tagObject) {

		ngMeta.setTitle(tagObject.title);
		ngMeta.setTag('description', tagObject.description);

	}

	//Check for parameters in URL and return boolean (useful for hiding/showing some things)
	$scope.urlParamIsPresent = function(key) {
		return (document.URL.indexOf(key) != -1);
	}

	$scope.setCatForSidebar = function(categoryTitle) {
		$scope.cat = categoryTitle;
	}

	//Format any int/double into a nicely displayable currency string
	$scope.prettyFormatPrice = function(rawPrice) {
		if(rawPrice == undefined || isNaN(rawPrice) || rawPrice == null) return "£0.00";

		if(typeof(rawPrice) == "string") rawPrice = parseInt(rawPrice);

		var snippet = "£" + rawPrice.toFixed(2);
		return snippet;
	}

	$scope.encodeHtml = function(html) {

		var newHtml = "";

		if(html != undefined) newHtml = html.replaceAll("£", "&pound;");

		return newHtml;

	}

	var feedbackIndex = 0;

	$scope.getFeedback = function() {
		return $scope.allFeedback[feedbackIndex];
	}

	$scope.applyAngularScope = function(callback) {
		if(!$scope.$$phase) {
	  		$scope.$apply()
		}

		if(callback != undefined) callback();
	}

	function handleCategories(tree) {
		//$scope.allCategories = tree;
		//$scope.$apply();
		window.setTimeout(scrollingSidebar, 2200);
	}

	$scope.navigateSidemenu = function(slug) {

	}

	$scope.runImageLoader = function() {

		//var waitLength = ($scope.firstProductNav) ? 250 : 0;

		//$scope.firstProductNav = false;

		var loadRepeater = 6,
		waitLength = 500;
		waitLengthIncrease = 120;

		function imageLoad() {

			$timeout(function() {

				$(".finalImage").one('load', function(e) {

					$(e.currentTarget).parent().find(".loaderImage").hide();
					$(e.currentTarget).fadeIn(500).css("display","block");

					window.setTimeout(function() {
						//$(e.currentTarget).parent().find(".stockbox").fadeIn(700);
						$(e.currentTarget).parent().find(".stockbox").fadeIn(200);

						$(".productwrap").unbind();

						$(".productwrap").hover(function() {

							var backImage = $(this).find(".prodGrid-backImage");

							if(backImage.data("loaded") == "loaded") {
								$(this).find(".prodGrid-finalImage").css("display", "none");
								backImage.css("display", "block");
							}

						}, function() {
							$(this).find(".prodGrid-backImage").css("display", "none");
							$(this).find(".prodGrid-finalImage").css("display", "block");
						});
					}, 500);


				}).each(function() {
				  if(this.complete) $(this).load();
				});

				if(loadRepeater > 1) {
					waitLength = waitLengthIncrease;
					loadRepeater--;
					imageLoad();
				}


			}, waitLength);

		}

		imageLoad();

	}

	function scrollingSidebar() {
		/*---Accordion sidemenu script---*/
        $(".sidebar-menu > li > div").click(function(){
            if(false == $(this).next().is(':visible')) {
                $('.sidebar-menu ul').slideUp(300);
            }
            $(this).next().slideToggle(300);
        });

        /*$(".mobile-sidebar-menu > li > div").click(function(){
            if(false == $(this).next().is(':visible')) {
                $('.mobile-sidebar-menu ul').slideUp(300);
            }
            $(this).next().slideToggle(300);
        });*/

        /*------ Scrolling sidebar snippet -----*/
        /*var element = $('.sidebar-box');
        var originalY = element.offset().top;

        // Space between element and top of screen 
        var topMargin = 25;

        var scrollTime = 300;

        element.css('position', 'relative');

        function scrollUpdate() {
            //Don't move side bar while on checkout page (distracting) or when on an iPad (buggy)
            if(document.URL.indexOf("/checkout") == -1 && navigator.userAgent.indexOf("iPad") == -1) {
                var scrollTop = $(window).scrollTop();

                var overlap = $("body").height() - $(window).scrollTop() - $(".footer-container").height() - $(".sidebar-box").height();

                var overlapPadding = 30;

                if(overlap < -overlapPadding) scrollTop += (overlap - overlapPadding);

                element.stop(false, false).animate({
                    top: scrollTop < originalY
                            ? 0
                            : scrollTop - originalY + topMargin
                }, scrollTime);
            }
        }

        $(window).on('scroll', scrollUpdate);

        $("#menu > li").click(function() {
            window.setTimeout(function() {
                scrollUpdate();
            }, 300);
        });*/

		var $sidebar   = $('.sidebar-box'), 
	        $window    = $(window),
	        offset     = $sidebar.offset(),
	        topPadding = 15;

	    $window.scroll(function() {
	        if ($window.scrollTop() > offset.top) {

	        	var moveMargin = $window.scrollTop() - offset.top + topPadding;

	        	var allContentHeight = $(".contentContainer").height() + $(".headerContainer").height();

	        	var sidebarHeight = $(".sidebar-box").height();

	        	if((sidebarHeight + moveMargin) < allContentHeight) {
	        		
		            $sidebar.stop().animate({
		                marginTop: $window.scrollTop() - offset.top + topPadding
		            });
	        		
	        	}

	        } else {
	            $sidebar.stop().animate({
	                marginTop: 0
	            });
	        }
	    });
    
	}

	$scope.runSearchQuery = function() {

		if(this.searchQuery.length > this.minSearchQueryLength) {
			//var path = "/filter?type=search&val=" + this.searchQuery;
			$location.path("/filter").search('type', 'search').search('val', this.searchQuery);

			eventTracking.runSearch();
		}
		/*var searchQuery = $("#search").val();

		if(searchQuery !== "" && searchQuery.length >= 3) {
			var formattedQuery = searchQuery.replaceAll(" ", "+");
			window.location.href = "search/" + formattedQuery;
		}*/

		//Bind search input to value and only display if field value is long enough
	}

	/*function productsCallback(products) {
		$scope.allProducts = products;
		console.log(products);
		$scope.$apply();
	}*/

	//Make moltin API calls when page loaded
	

	/*$scope.applyScope = function() {
		try {
			$scope.$apply();
		} catch(e) { 
			//scope applying not necessary
		}
	}*/

	function updateOpeningSign() {

		var date = new Date(),
		currentDay = date.getDay(),
		currentHours = date.getHours(),
		currentMinutes = date.getMinutes(),
		opening, openHour, currentlyOpen, openUntil;

		if(currentDay != 0) {

			if(currentHours < 10) {
				currentlyOpen = false;
				opening = "today";
				openHour = "10:00";
			} else if(currentHours >= 10 && currentHours <= 18) {
				currentlyOpen = true;
				openUntil = "18:00";
			} else if(currentHours > 18) {
				currentlyOpen = false;
				opening = "tomorrow";
				if(currentDay < 6) {
					openHour = "10:00";
				} else {
					openHour = "11:00";
				}
			}  

		} else {

			if(currentHours < 11) {
				currentlyOpen = false;
				opening = "today";
				openHour = "11:00";
			} else if(currentHours >= 11 && (currentHours < 17 || (currentHours == 17 && currentMinutes <= 30))) {
				currentlyOpen = true;
				openUntil = "17:30";
			} else if(currentHours >= 17 && currentMinutes > 30) {
				currentlyOpen = false;
				opening = "tomorrow";
				openHour = "10:00";				
			}			

		}

		var openText;

		if(currentlyOpen) {
			openText = "Until " + openUntil;
		} else {
			openText = "Opening " + opening + " at " + openHour;
		}

		if(currentlyOpen) {
			$(".footer-openingSign").html("<h4>OPEN NOW</h4>");
		} else {
			$(".footer-openingSign").html("<h4>CLOSED</h4>");
		}

		$(".footer-openingText").html("- " + openText);

	}

	//Check cookies if need be
	$scope.runCookieCheck = function(cookies) {

		//Check login cookie and login user
		console.log("Checking login cookies");

		var cookieJson = cookies.getObject("editedWebstoreCookieCache");

		if(cookieJson != undefined) {
			$scope.loggedIn = true;
			$scope.loggedInCustomerJson = cookieJson;
		}

	}


	//Pass cookies object through here because for whatever reason the function doesn't have access!
	$scope.runCookieCheck($cookies);

	$scope.loginUser = function(userJson) {
		//wrapper function to handle user login, should also be run at launch of site to check login cookies
		$scope.loggedIn = true;
		$scope.loggedInCustomerJson = userJson;
	}

	$scope.logoutUser = function() {

		function logoutCallback(data) {
			//Clear login detail and login cookie
			$cookies.remove("editedWebstoreCookieCache");
			$scope.loggedIn = false;
			$scope.loggedInCustomerJson = null;
			//Clear customer addresses (if loaded)
			$scope.loggedInCustomerAddresses = null;
			$scope.loggedInAddressesLoaded = false;
		}

		function logoutFailureCallback() {
			console.log("failed to log out");
		}

		var logoutJson = {
			token: $scope.loggedInCustomerJson.token
		};

		eventTracking.logout();

		remoteRequestService.logoutCustomerCall(logoutJson, logoutCallback, logoutFailureCallback);

	}

	//Global customer address loading
	$scope.loadCustomerAddresses = function(extraCallback) {

		function customerAddressCallback(data) {

			var addressArray = data.data.result;

			$scope.loggedInCustomerAddresses = addressArray;
			$scope.loggedInAddressesLoaded = true;

			//Extra callback necessary for the checkout
			if(extraCallback !== undefined) extraCallback();

		}

		function customerAddressFailureCallback(error) {

		}

		if($scope.loggedIn) remoteRequestService.getAddressesForCustomer($scope.loggedInCustomerJson.id, $scope.loggedInCustomerJson.token, customerAddressCallback, customerAddressFailureCallback);

	}

	//Update the quantity of an item in the user cart
	$scope.cart_updateQuantity = function(buttonId, key, oldQuantity) {
		var buttonId = "#numberField-" + buttonId;

		var quantity = this.singleItemQuantity;

		if(quantity !== oldQuantity) {
			if($scope.urlParamIsPresent("checkout") && (quantity == undefined || quantity < 1)) {
				$(buttonId).val(1)
			} else if(quantity == 0) {
				//Remove item from cart, no quantity
				$scope.makeUserRequest({'op': 'removeFromCart', 'cart_item_id': key });
			} else {
				$scope.makeUserRequest({'op': 'updateCartQuantity', 'cart_item_id': key, "quantity": quantity });
			}
		}
	}

	$scope.cart_calculateEstimatedTotal = function() {
		var total = $scope.userCart.totals.post_discount.raw.with_tax;

		$scope.calculateFreeDelivery(total);

		if(!$scope.app_qualifiedForFreeDelivery) {
			total += 4.95;
		}

		$scope.app_cartEstimatedTotal = total;
	}

	$scope.cart_applyPromoCode = function(code) {

		$scope.makeUserRequest({'op': 'addPromoCode', 'code': code})

	}

	$scope.calculateFreeDelivery = function(total) {

		var text;

		if(total >= $scope.freeDeliveryAmount) {
			$scope.app_qualifiedForFreeDelivery = true;
			text = "You've spent over £30 so we will pick up the cost of your delivery.";
		} else {
			$scope.app_qualifiedForFreeDelivery = false;
			text = "Spend just £30 and delivery is on us!";
		}

		$scope.freeDeliveryText = text;

	}

	$scope.addItemToCart = function(productId, productName, quantity, callback) {

		if($scope.cartAddingInProgress) return;

			//$scope.itemJustAdded = true;

		$scope.cartAddingInProgress = true;

		function addItemComplete() {
			$scope.cartAddingInProgress = false;

			eventTracking.addToCart(productName);

			callback();
		}


		$scope.makeUserRequest({'op': 'addToCart', 'product_id': productId, 'quantity': quantity }, addItemComplete); 
	}

	/*$scope.setCartAnimPos = function() {

		$(".cartAnimObj").css("background", "transparent");
		var basketButtonPos = $(".product-addToCart").offset();

		if(basketButtonPos != undefined) {
			$(".cartAnimObj").css("top", basketButtonPos.top);
			$(".cartAnimObj").css("left", basketButtonPos.left + 10);
		}
	}*/

	//Make a request to the moltin API service which handles all kinds of balls
	$scope.makeUserRequest = function(requestObject, extraCallback) {

		function applyScope() {
			$scope.$apply();
		}

		switch(requestObject.op) {

			case "addToCart":

				function addToCartCallback(item) {
					$scope.makeUserRequest({op: "retreiveCart"}, extraCallback);

					//if(extraCallback != undefined) extraCallback();
				}

				moltinApiService.addProductToCart(requestObject.product_id, requestObject.quantity, addToCartCallback);

			break;

			case "retreiveCart":

				function retreiveCartCallback(cart) {
					$scope.rawCartAmount = cart.totals.post_discount.formatted.with_tax;
					$scope.userCart = cart;

					$scope.app_cartUpdating = false;

					//Calculate estimated total here as cart needs to be loaded before any calculation can be done
					$scope.cart_calculateEstimatedTotal();

					if(cart.discount_code != null) {
						$scope.promo_code.active = true;
						$scope.promo_code.code = cart.discount_code;
						$scope.promo_code.discount = cart.discount_code.match(/[0-9]+/)[0] + "%";
					}

					applyScope();

					if(extraCallback != undefined) extraCallback();

				}

				$scope.app_cartUpdating = true;

				moltinApiService.getCartData(null, retreiveCartCallback);

			break;

			case "removeFromCart":

				function removeFromCartCallBack() {
					$scope.makeUserRequest({op: "retreiveCart"});

					eventTracking.removeFromCart();
				}

				//Set cart updating to prevent additional input
				$scope.app_cartUpdating = true;

				moltinApiService.removeFromCart(requestObject.cart_item_id, removeFromCartCallBack);

			break;

			case "clearCart":

				function clearCartCallBack() {
					$scope.rawCartAmount = "£0.00";
					$scope.userCart = null;

					$scope.app_cartUpdating = false;

					eventTracking.clearCart();

					applyScope();
				}

				//Set cart updating to prevent additional input
				$scope.app_cartUpdating = true;

				moltinApiService.clearCart(clearCartCallBack);

			break;

			case "updateCartQuantity":

				function updateCartQuantityCallback() {
					$scope.makeUserRequest({op: "retreiveCart"});
				}

				//Set cart updating to prevent additional input
				$scope.app_cartUpdating = true;

				moltinApiService.updateCartQuantity(requestObject.cart_item_id, requestObject.quantity, updateCartQuantityCallback);

			break;

			case "addPromoCode":

				function addPromoCodeCallback() {
					$scope.makeUserRequest({op: "retreiveCart"});
				}

				function addPromoCodeErrorCallback() {
					console.log("not valid code");
					$scope.app_cartUpdating = false;

					applyScope();
				}

				$scope.app_cartUpdating = true;

				moltinApiService.applyPromoCode(requestObject.code, addPromoCodeCallback, addPromoCodeErrorCallback);

		}



	}

	$scope.contact_sendContactMessage = function(contactObject) {
		$scope.contact_stage = "sending";

		function successCallback() {

			$scope.contact_stage = "complete";

		}

		function failureCallback() {

			$scope.contact_stage = "failed";

		}

		remoteRequestService.sendContactFormEmail(contactObject, successCallback, failureCallback);

	}



}])


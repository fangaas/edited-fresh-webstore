app.service('editedFurnitureModifierStore', function() {

	/*
	Upon loading up a furniture with modifiers and potential variants

	- Sort out groupings 
	- Display modifiers
	- User selects modifiers and once filled add to cart
	- When adding to cart, convert modifiers to object 
	- Either place modifier into cart via description or store them in an object in local storage
	- When at checkout display product modifiers chosen and append colour swatch price increase to the item
	- Then send modifiers in email receipt to us and customer


	Alternately considering colour price increase

	- When selecting product modifiers, choose a colour group and a colour
	- Add the colour group as a 'product' to make it easier to check out the cart without hacking the product cost
	- Each colour group can have a variant based on size e.g. steel finish large items £10
	*/

	/*this.furnitureModifiers = {
		"hay-revolver-stool-65cm-red": [{
			modifier: "Base Colour",
			type: "colour",
			colourGroups: [{
				id: "remix2",
				priceIncrease: 50
			}]
		}, {
			modifier: "Leg Colour",
			type: "colour",
			colourSwatch: "colour group name"
		}, {
			//Placeholder possibly not needed on first iteration
			modifier: "Yes/no option",
			type: "combo",
			selectedByDefault: true
		}]
	}*/

	this.furnitureModifiers = {
		"byalex-a-table-grey": [{
			modifier: "Base Colour",
			type: "colour",
			colourGroups: [{
				id: "remix2",
				priceIncrease: 50
			}]
		}]
	}
	
	//Colour swatch structure, likely to stay the same, images stored under images/colour_store_images, full image path not necessary
	this.colourGroups = {
		"remix2": {
			name: "Remix 2",
			colours: [113,123,133,143,152,163,173,183,223,233,242,252,362,373,383,393,412,422,433,443]
		}
	}

});

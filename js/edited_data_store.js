app.service('editedDataStore', function() {

    //Popular products list for home page
    this.homeProducts = [{
        onSale: false,
        title: 'Milano Collection - Six Luxury Crackers',
        image: 'https://www.data.edited.co.uk/images/product/m/milano-collection-six-luxury-crackers-600px-600px.png',
        price: '£40.00',
        slug: 'milano-collection-six-luxury-crackers'
    },{
        onSale: false,
        title: 'Jingle All The Way Crackers - Set of 6 Christmas Crackers',
        image: 'https://www.data.edited.co.uk/images/product/j/jingle-all-the-way-crackers-set-of-6-christmas-crackers-600px-600px.png',
        price: '£15.00',
        slug: 'jingle-all-the-way-crackers-set-of-6-christmas-crackers'
    },{
        onSale: false,
        title: 'HAY - Nathalie Du Pasquier Cushion - Square - Penta',
        image: 'https://www.data.edited.co.uk/images/product/h/hay-nathalie-du-pasquier-cushion-square-penta-600px-600px.png',
        price: '£50.00',
        slug: 'hay-nathalie-du-pasquier-cushion-square-penta'
    }];

    /*
    {
        onSale: ,
        title: ,
        image: ,
        price: ,
        slug: 
    }
    */

    this.allFeedback = [{
        text: 'I was exceptionally pleased with the whole experience of shopping with Edited. The coasters I bought were priced competitively and arrived promptly and well packed.',
        name: 'Paul Sandbach'
    }];

    this.allCategories =  [{
        "id": "1181263686031901379",
        "order": 3,
        "created_at": "2016-02-09 17:07:03",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Christmas",
            "data": {
                "id": "1181263000623907520",
                "order": 10,
                "created_at": "2016-02-09 17:05:41",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "christmas",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Christmas",
                "description": "Christmas"
            }
        },
        "slug": "christmas-gift-boxes",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Gift Boxes",
        "description": "Gift Boxes",
        "images": []
    }, {
        "id": "1181263505400005314",
        "order": 2,
        "created_at": "2016-02-09 17:06:41",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Christmas",
            "data": {
                "id": "1181263000623907520",
                "order": 10,
                "created_at": "2016-02-09 17:05:41",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "christmas",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Christmas",
                "description": "Christmas"
            }
        },
        "slug": "christmas-crackers",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Crackers",
        "description": "Crackers",
        "images": []
    }, {
        "id": "1181263290324484801",
        "order": 1,
        "created_at": "2016-02-09 17:06:16",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Christmas",
            "data": {
                "id": "1181263000623907520",
                "order": 10,
                "created_at": "2016-02-09 17:05:41",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "christmas",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Christmas",
                "description": "Christmas"
            }
        },
        "slug": "christmas-decorations",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Decorations",
        "description": "Decorations",
        "images": []
    }, {
        "id": "1181263000623907520",
        "order": 10,
        "created_at": "2016-02-09 17:05:41",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "christmas",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Christmas",
        "description": "Christmas",
        "images": []
    }, {
        "id": "1181262788157244095",
        "order": 3,
        "created_at": "2016-02-09 17:05:16",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Utility",
            "data": {
                "id": "1181262213478875836",
                "order": 9,
                "created_at": "2016-02-09 17:04:07",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "utility",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Utility",
                "description": "Utility"
            }
        },
        "slug": "utility-accessories",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Accessories",
        "description": "Accessories",
        "images": []
    }, {
        "id": "1181262595680633534",
        "order": 2,
        "created_at": "2016-02-09 17:04:53",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Utility",
            "data": {
                "id": "1181262213478875836",
                "order": 9,
                "created_at": "2016-02-09 17:04:07",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "utility",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Utility",
                "description": "Utility"
            }
        },
        "slug": "utility-telephones",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Telephones",
        "description": "Telephones",
        "images": []
    }, {
        "id": "1181262414839022269",
        "order": 1,
        "created_at": "2016-02-09 17:04:31",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Utility",
            "data": {
                "id": "1181262213478875836",
                "order": 9,
                "created_at": "2016-02-09 17:04:07",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "utility",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Utility",
                "description": "Utility"
            }
        },
        "slug": "utility-storage",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Storage",
        "description": "Storage",
        "images": []
    }, {
        "id": "1181262213478875836",
        "order": 9,
        "created_at": "2016-02-09 17:04:07",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "utility",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Utility",
        "description": "Utility",
        "images": []
    }, {
        "id": "1181262070100787899",
        "order": 8,
        "created_at": "2016-02-09 17:03:50",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "gifts",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Gifts",
        "description": "Gifts",
        "images": []
    }, {
        "id": "1181261949766206138",
        "order": 3,
        "created_at": "2016-02-09 17:03:36",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Stationery",
            "data": {
                "id": "1181261179582939827",
                "order": 7,
                "created_at": "2016-02-09 17:02:04",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "stationery",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Stationery",
                "description": "Stationery"
            }
        },
        "slug": "stationery-notebooks-and-files",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Notebooks & Files",
        "description": "Notebooks & Files",
        "images": []
    }, {
        "id": "1181261681808900793",
        "order": 2,
        "created_at": "2016-02-09 17:03:04",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Stationery",
            "data": {
                "id": "1181261179582939827",
                "order": 7,
                "created_at": "2016-02-09 17:02:04",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "stationery",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Stationery",
                "description": "Stationery"
            }
        },
        "slug": "stationery-greetings-cards",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Greetings Cards",
        "description": "Greetings Cards",
        "images": []
    }, {
        "id": "1181261350089786036",
        "order": 1,
        "created_at": "2016-02-09 17:02:24",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Stationery",
            "data": {
                "id": "1181261179582939827",
                "order": 7,
                "created_at": "2016-02-09 17:02:04",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "stationery",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Stationery",
                "description": "Stationery"
            }
        },
        "slug": "stationery-accessories",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Accessories",
        "description": "Accessories",
        "images": []
    }, {
        "id": "1181261179582939827",
        "order": 7,
        "created_at": "2016-02-09 17:02:04",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "stationery",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Stationery",
        "description": "Stationery",
        "images": []
    }, {
        "id": "1181261006316241584",
        "order": 6,
        "created_at": "2016-02-09 17:01:43",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Lighting",
            "data": {
                "id": "1181259843982328490",
                "order": 6,
                "created_at": "2016-02-09 16:59:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "lighting",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Lighting",
                "description": "Lighting"
            }
        },
        "slug": "lighting-fairy-lights",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Fairy Lights",
        "description": "Fairy Lights",
        "images": []
    }, {
        "id": "1181260789521056431",
        "order": 5,
        "created_at": "2016-02-09 17:01:17",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Lighting",
            "data": {
                "id": "1181259843982328490",
                "order": 6,
                "created_at": "2016-02-09 16:59:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "lighting",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Lighting",
                "description": "Lighting"
            }
        },
        "slug": "lighting-accessories",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Accessories",
        "description": "Accessories",
        "images": []
    }, {
        "id": "1181260556938511022",
        "order": 4,
        "created_at": "2016-02-09 17:00:50",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Lighting",
            "data": {
                "id": "1181259843982328490",
                "order": 6,
                "created_at": "2016-02-09 16:59:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "lighting",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Lighting",
                "description": "Lighting"
            }
        },
        "slug": "lighting-lightbulbs",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Lightbulbs",
        "description": "Lightbulbs",
        "images": []
    }, {
        "id": "1181260383160107693",
        "order": 3,
        "created_at": "2016-02-09 17:00:29",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Lighting",
            "data": {
                "id": "1181259843982328490",
                "order": 6,
                "created_at": "2016-02-09 16:59:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "lighting",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Lighting",
                "description": "Lighting"
            }
        },
        "slug": "lighting-lampshades",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Lampshades",
        "description": "Lampshades",
        "images": []
    }, {
        "id": "1181260202293330604",
        "order": 2,
        "created_at": "2016-02-09 17:00:07",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Lighting",
            "data": {
                "id": "1181259843982328490",
                "order": 6,
                "created_at": "2016-02-09 16:59:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "lighting",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Lighting",
                "description": "Lighting"
            }
        },
        "slug": "lighting-table-and-floor-lamps",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Table & Floor Lamps",
        "description": "Table & Floor Lamps",
        "images": []
    }, {
        "id": "1181260028674310827",
        "order": 1,
        "created_at": "2016-02-09 16:59:47",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Lighting",
            "data": {
                "id": "1181259843982328490",
                "order": 6,
                "created_at": "2016-02-09 16:59:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "lighting",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Lighting",
                "description": "Lighting"
            }
        },
        "slug": "lighting-ceiling-pendants",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Ceiling Pendants",
        "description": "Ceiling Pendants",
        "images": []
    }, {
        "id": "1181259843982328490",
        "order": 6,
        "created_at": "2016-02-09 16:59:25",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "lighting",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Lighting",
        "description": "Lighting",
        "images": []
    }, {
        "id": "1181259514435863209",
        "order": 2,
        "created_at": "2016-02-09 16:58:45",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Furniture",
            "data": {
                "id": "1181259123786777255",
                "order": 5,
                "created_at": "2016-02-09 16:57:59",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "furniture",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Furniture",
                "description": "Furniture"
            }
        },
        "slug": "furniture-beanbags",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Beanbags",
        "description": "Beanbags",
        "images": []
    }, {
        "id": "1181259303261045416",
        "order": 1,
        "created_at": "2016-02-09 16:58:20",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Furniture",
            "data": {
                "id": "1181259123786777255",
                "order": 5,
                "created_at": "2016-02-09 16:57:59",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "furniture",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Furniture",
                "description": "Furniture"
            }
        },
        "slug": "furniture-tables",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Tables",
        "description": "Tables",
        "images": []
    }, {
        "id": "1181259123786777255",
        "order": 5,
        "created_at": "2016-02-09 16:57:59",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "furniture",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Furniture",
        "description": "Furniture",
        "images": []
    }, {
        "id": "1181258920321090214",
        "order": 4,
        "created_at": "2016-02-09 16:57:35",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "prints",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Prints",
        "description": "Prints",
        "images": []
    }, {
        "id": "1181258762195829413",
        "order": 5,
        "created_at": "2016-02-09 16:57:16",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Soft Furnishings",
            "data": {
                "id": "1181257728819987104",
                "order": 3,
                "created_at": "2016-02-09 16:55:13",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "soft-furnishings",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Soft Furnishings",
                "description": "Soft Furnishings"
            }
        },
        "slug": "soft-furnishings-towels",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Towels",
        "description": "Towels",
        "images": []
    }, {
        "id": "1181258570306421412",
        "order": 4,
        "created_at": "2016-02-09 16:56:53",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Soft Furnishings",
            "data": {
                "id": "1181257728819987104",
                "order": 3,
                "created_at": "2016-02-09 16:55:13",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "soft-furnishings",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Soft Furnishings",
                "description": "Soft Furnishings"
            }
        },
        "slug": "soft-furnishings-rugs",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Rugs",
        "description": "Rugs",
        "images": []
    }, {
        "id": "1181258386990170787",
        "order": 3,
        "created_at": "2016-02-09 16:56:31",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Soft Furnishings",
            "data": {
                "id": "1181257728819987104",
                "order": 3,
                "created_at": "2016-02-09 16:55:13",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "soft-furnishings",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Soft Furnishings",
                "description": "Soft Furnishings"
            }
        },
        "slug": "soft-furnishings-bedding",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Bedding",
        "description": "Bedding",
        "images": []
    }, {
        "id": "1181258214654608034",
        "order": 2,
        "created_at": "2016-02-09 16:56:10",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Soft Furnishings",
            "data": {
                "id": "1181257728819987104",
                "order": 3,
                "created_at": "2016-02-09 16:55:13",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "soft-furnishings",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Soft Furnishings",
                "description": "Soft Furnishings"
            }
        },
        "slug": "soft-furnishings-throws",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Throws",
        "description": "Throws",
        "images": []
    }, {
        "id": "1181257916724806305",
        "order": 1,
        "created_at": "2016-02-09 16:55:35",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Soft Furnishings",
            "data": {
                "id": "1181257728819987104",
                "order": 3,
                "created_at": "2016-02-09 16:55:13",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "soft-furnishings",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Soft Furnishings",
                "description": "Soft Furnishings"
            }
        },
        "slug": "soft-furnishings-cushions",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Cushions",
        "description": "Cushions",
        "images": []
    }, {
        "id": "1181257728819987104",
        "order": 3,
        "created_at": "2016-02-09 16:55:13",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "soft-furnishings",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Soft Furnishings",
        "description": "Soft Furnishings",
        "images": []
    }, {
        "id": "1181257475341419167",
        "order": 1,
        "created_at": "2016-02-09 16:54:42",
        "updated_at": "2016-07-11 11:08:10",
        "parent": {
            "value": "Kitchen & Dining",
            "data": {
                "id": "1181256741984141979",
                "order": 11,
                "created_at": "2016-02-09 16:53:15",
                "updated_at": "2016-07-11 10:50:56",
                "parent": null,
                "slug": "kitchen-and-dining",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Kitchen & Dining",
                "description": "Kitchen & Dining"
            }
        },
        "slug": "kitchen-and-dining-tea-towels-and-textiles",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Tea Towels & Textiles",
        "description": "Tea Towels & Textiles",
        "images": []
    }, {
        "id": "1181257254611976862",
        "order": 4,
        "created_at": "2016-02-09 16:54:16",
        "updated_at": "2016-07-11 11:09:02",
        "parent": {
            "value": "Kitchen & Dining",
            "data": {
                "id": "1181256741984141979",
                "order": 11,
                "created_at": "2016-02-09 16:53:15",
                "updated_at": "2016-07-11 10:50:56",
                "parent": null,
                "slug": "kitchen-and-dining",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Kitchen & Dining",
                "description": "Kitchen & Dining"
            }
        },
        "slug": "kitchen-and-dining-serveware",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Serveware",
        "description": "Serveware",
        "images": []
    }, {
        "id": "1181257094758662813",
        "order": 3,
        "created_at": "2016-02-09 16:53:57",
        "updated_at": "2016-07-11 11:08:49",
        "parent": {
            "value": "Kitchen & Dining",
            "data": {
                "id": "1181256741984141979",
                "order": 11,
                "created_at": "2016-02-09 16:53:15",
                "updated_at": "2016-07-11 10:50:56",
                "parent": null,
                "slug": "kitchen-and-dining",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Kitchen & Dining",
                "description": "Kitchen & Dining"
            }
        },
        "slug": "kitchen-and-dining-glassware",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Glassware",
        "description": "Glassware",
        "images": []
    }, {
        "id": "1181256910033126044",
        "order": 2,
        "created_at": "2016-02-09 16:53:35",
        "updated_at": "2016-07-11 11:08:37",
        "parent": {
            "value": "Kitchen & Dining",
            "data": {
                "id": "1181256741984141979",
                "order": 11,
                "created_at": "2016-02-09 16:53:15",
                "updated_at": "2016-07-11 10:50:56",
                "parent": null,
                "slug": "kitchen-and-dining",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Kitchen & Dining",
                "description": "Kitchen & Dining"
            }
        },
        "slug": "kitchen-and-dining-cups-and-mugs",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Cups & Mugs",
        "description": "Cups & Mugs",
        "images": []
    }, {
        "id": "1181256741984141979",
        "order": 11,
        "created_at": "2016-02-09 16:53:15",
        "updated_at": "2016-07-11 10:50:56",
        "parent": null,
        "slug": "kitchen-and-dining",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Kitchen & Dining",
        "description": "Kitchen & Dining",
        "images": []
    }, {
        "id": "1181256540925985434",
        "order": 5,
        "created_at": "2016-02-09 16:52:51",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Accessories",
            "data": {
                "id": "1180513910940237847",
                "order": 2,
                "created_at": "2016-02-08 16:17:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "accessories",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Accessories",
                "description": "Accessories"
            }
        },
        "slug": "accessories-plant-pots",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Plant Pots",
        "description": "Plant Pots",
        "images": []
    }, {
        "id": "1181256251963605657",
        "order": 4,
        "created_at": "2016-02-09 16:52:17",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Accessories",
            "data": {
                "id": "1180513910940237847",
                "order": 2,
                "created_at": "2016-02-08 16:17:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "accessories",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Accessories",
                "description": "Accessories"
            }
        },
        "slug": "accessories-clocks",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Clocks",
        "description": "Clocks",
        "images": []
    }, {
        "id": "1181256096833077912",
        "order": 3,
        "created_at": "2016-02-09 16:51:58",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Accessories",
            "data": {
                "id": "1180513910940237847",
                "order": 2,
                "created_at": "2016-02-08 16:17:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "accessories",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Accessories",
                "description": "Accessories"
            }
        },
        "slug": "accessories-home-storage",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Home Storage",
        "description": "Home Storage",
        "images": []
    }, {
        "id": "1181255941350228631",
        "order": 2,
        "created_at": "2016-02-09 16:51:39",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Accessories",
            "data": {
                "id": "1180513910940237847",
                "order": 2,
                "created_at": "2016-02-08 16:17:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "accessories",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Accessories",
                "description": "Accessories"
            }
        },
        "slug": "accessories-home-decoration",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Home Decoration",
        "description": "Home Decoration",
        "images": []
    }, {
        "id": "1180514173637885976",
        "order": 1,
        "created_at": "2016-02-08 16:17:56",
        "updated_at": "2016-06-03 11:02:13",
        "parent": {
            "value": "Accessories",
            "data": {
                "id": "1180513910940237847",
                "order": 2,
                "created_at": "2016-02-08 16:17:25",
                "updated_at": "2016-06-03 11:02:13",
                "parent": null,
                "slug": "accessories",
                "status": {
                    "value": "Live",
                    "data": {
                        "key": "1",
                        "value": "Live"
                    }
                },
                "title": "Accessories",
                "description": "Accessories"
            }
        },
        "slug": "accessories-candles-scents-and-holders",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Candles, Scents & Holders",
        "description": "Candles, Scents & Holders",
        "images": []
    }, {
        "id": "1180513910940237847",
        "order": 2,
        "created_at": "2016-02-08 16:17:25",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "accessories",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Accessories",
        "description": "Accessories",
        "images": []
    }, {
        "id": "1176187812504928780",
        "order": 1,
        "created_at": "2016-02-02 17:02:16",
        "updated_at": "2016-06-03 11:02:13",
        "parent": null,
        "slug": "uncategorized",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Uncategorized",
        "description": "Products that do not fit into another category",
        "images": []
    }];

    this.allBrands = [{
        "id": "1252841240215421062",
        "order": null,
        "created_at": "2016-05-18 11:18:56",
        "updated_at": "2016-05-18 11:18:56",
        "slug": "The-Pineapple-Co",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "The Pineapple Co",
        "description": "The Pineapple Co",
        "images": []
    }, {
        "id": "1227557746610537020",
        "order": null,
        "created_at": "2016-04-13 14:05:09",
        "updated_at": "2016-04-13 14:05:09",
        "slug": "Kreafunk",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Kreafunk",
        "description": "Kreafunk",
        "images": []
    }, {
        "id": "1205691810626667316",
        "order": null,
        "created_at": "2016-03-14 10:01:25",
        "updated_at": "2016-03-14 10:01:25",
        "slug": "Danica-Studio",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Danica Studio",
        "description": "Danica Studio",
        "images": []
    }, {
        "id": "1197010709506949617",
        "order": null,
        "created_at": "2016-03-02 10:33:37",
        "updated_at": "2016-03-02 10:33:37",
        "slug": "Edited",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Edited",
        "description": "Edited",
        "images": []
    }, {
        "id": "1192084798428939067",
        "order": null,
        "created_at": "2016-02-24 15:26:42",
        "updated_at": "2016-02-24 15:26:42",
        "slug": "unbranded",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Unbranded",
        "description": "Products with no specific brand.",
        "images": []
    }, {
        "id": "1191954953027978018",
        "order": null,
        "created_at": "2016-02-24 11:08:44",
        "updated_at": "2016-02-24 11:08:44",
        "slug": "Fred",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Fred",
        "description": "Fred",
        "images": []
    }, {
        "id": "1191954948892394273",
        "order": null,
        "created_at": "2016-02-24 11:08:43",
        "updated_at": "2016-02-24 11:08:43",
        "slug": "NUD-Collection",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "NUD Collection",
        "description": "NUD Collection",
        "images": []
    }, {
        "id": "1191954943649514272",
        "order": null,
        "created_at": "2016-02-24 11:08:42",
        "updated_at": "2016-02-24 11:08:42",
        "slug": "Pucket",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Pucket",
        "description": "Pucket",
        "images": []
    }, {
        "id": "1191954934673703711",
        "order": null,
        "created_at": "2016-02-24 11:08:41",
        "updated_at": "2016-02-24 11:08:41",
        "slug": "Rocket-&-Rye",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Rocket & Rye",
        "description": "Rocket & Rye",
        "images": []
    }, {
        "id": "1191954930361959198",
        "order": null,
        "created_at": "2016-02-24 11:08:41",
        "updated_at": "2016-02-24 11:08:41",
        "slug": "Ester&Erik",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Ester&Erik",
        "description": "Ester&Erik",
        "images": []
    }, {
        "id": "1191954925135856413",
        "order": null,
        "created_at": "2016-02-24 11:08:40",
        "updated_at": "2016-02-24 11:08:40",
        "slug": "Boskke",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Boskke",
        "description": "Boskke",
        "images": []
    }, {
        "id": "1191954919540654876",
        "order": null,
        "created_at": "2016-02-24 11:08:40",
        "updated_at": "2016-02-24 11:08:40",
        "slug": "Ohh-Deer",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Ohh Deer",
        "description": "Ohh Deer",
        "images": []
    }, {
        "id": "1191954914733982491",
        "order": null,
        "created_at": "2016-02-24 11:08:39",
        "updated_at": "2016-02-24 11:08:39",
        "slug": "Plumen",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Plumen",
        "description": "Plumen",
        "images": []
    }, {
        "id": "1191954908794848026",
        "order": null,
        "created_at": "2016-02-24 11:08:38",
        "updated_at": "2016-02-24 11:08:38",
        "slug": "Paddywax",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Paddywax",
        "description": "Paddywax",
        "images": []
    }, {
        "id": "1191954904088838937",
        "order": null,
        "created_at": "2016-02-24 11:08:38",
        "updated_at": "2016-02-24 11:08:38",
        "slug": "Meri-Meri",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Meri Meri",
        "description": "Meri Meri",
        "images": []
    }, {
        "id": "1191954899718374168",
        "order": null,
        "created_at": "2016-02-24 11:08:37",
        "updated_at": "2016-02-24 11:08:37",
        "slug": "ThinkGadget",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "ThinkGadget",
        "description": "ThinkGadget",
        "images": []
    }, {
        "id": "1191954894249001751",
        "order": null,
        "created_at": "2016-02-24 11:08:37",
        "updated_at": "2016-02-24 11:08:37",
        "slug": "Nook-London",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Nook London",
        "description": "Nook London",
        "images": []
    }, {
        "id": "1191954889224225558",
        "order": null,
        "created_at": "2016-02-24 11:08:36",
        "updated_at": "2016-02-24 11:08:36",
        "slug": "Coach-House",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Coach House",
        "description": "Coach House",
        "images": []
    }, {
        "id": "1191954875416576789",
        "order": null,
        "created_at": "2016-02-24 11:08:34",
        "updated_at": "2016-02-24 11:08:34",
        "slug": "Mini-Moderns",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Mini Moderns",
        "description": "Mini Moderns",
        "images": []
    }, {
        "id": "1191954852566008596",
        "order": null,
        "created_at": "2016-02-24 11:08:32",
        "updated_at": "2016-02-24 11:08:32",
        "slug": "thomaspaul",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "thomaspaul",
        "description": "thomaspaul",
        "images": []
    }, {
        "id": "1191954831066006291",
        "order": null,
        "created_at": "2016-02-24 11:08:29",
        "updated_at": "2016-02-24 11:08:29",
        "slug": "Seletti",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Seletti",
        "description": "Seletti",
        "images": []
    }, {
        "id": "1191954807225582354",
        "order": null,
        "created_at": "2016-02-24 11:08:26",
        "updated_at": "2016-02-24 11:08:26",
        "slug": "Seven-Gauge-Studios",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Seven Gauge Studios",
        "description": "Seven Gauge Studios",
        "images": []
    }, {
        "id": "1191954783091557137",
        "order": null,
        "created_at": "2016-02-24 11:08:23",
        "updated_at": "2016-02-24 11:08:23",
        "slug": "Ferm-Living",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Ferm Living",
        "description": "Ferm Living",
        "images": []
    }, {
        "id": "1191954764796003088",
        "order": null,
        "created_at": "2016-02-24 11:08:21",
        "updated_at": "2016-02-24 11:08:21",
        "slug": "Newgate-Clocks",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Newgate Clocks",
        "description": "Newgate Clocks",
        "images": []
    }, {
        "id": "1191954747054097167",
        "order": null,
        "created_at": "2016-02-24 11:08:19",
        "updated_at": "2016-02-24 11:08:19",
        "slug": "Krebs",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Krebs",
        "description": "Krebs",
        "images": []
    }, {
        "id": "1191954729119253262",
        "order": null,
        "created_at": "2016-02-24 11:08:17",
        "updated_at": "2016-02-24 11:08:17",
        "slug": "ByALEX",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "ByALEX",
        "description": "ByALEX",
        "images": []
    }, {
        "id": "1191954712115544845",
        "order": null,
        "created_at": "2016-02-24 11:08:15",
        "updated_at": "2016-02-24 11:08:15",
        "slug": "Normann-Copenhagen",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Normann Copenhagen",
        "description": "Normann Copenhagen",
        "images": []
    }, {
        "id": "1191954696948941580",
        "order": null,
        "created_at": "2016-02-24 11:08:13",
        "updated_at": "2016-02-24 11:08:13",
        "slug": "Sarah-Edmonds-Illustration",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Sarah Edmonds Illustration",
        "description": "Sarah Edmonds Illustration",
        "images": []
    }, {
        "id": "1191954684022096651",
        "order": null,
        "created_at": "2016-02-24 11:08:12",
        "updated_at": "2016-02-24 11:08:12",
        "slug": "The-Lost-Fox",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "The Lost Fox",
        "description": "The Lost Fox",
        "images": []
    }, {
        "id": "1191954671153971978",
        "order": null,
        "created_at": "2016-02-24 11:08:10",
        "updated_at": "2016-02-24 11:08:10",
        "slug": "HAY",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "HAY",
        "description": "HAY",
        "images": []
    }, {
        "id": "1191954659879682825",
        "order": null,
        "created_at": "2016-02-24 11:08:09",
        "updated_at": "2016-02-24 11:08:09",
        "slug": "Wrong-for-HAY",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Wrong for HAY",
        "description": "Wrong for HAY",
        "images": []
    }, {
        "id": "1191954651474297608",
        "order": null,
        "created_at": "2016-02-24 11:08:08",
        "updated_at": "2016-02-24 11:08:08",
        "slug": "Ekobo",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Ekobo",
        "description": "Ekobo",
        "images": []
    }, {
        "id": "1191954646508241671",
        "order": null,
        "created_at": "2016-02-24 11:08:07",
        "updated_at": "2016-02-24 11:08:07",
        "slug": "Woouf",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Woouf",
        "description": "Woouf",
        "images": []
    }, {
        "id": "1191954637465322246",
        "order": null,
        "created_at": "2016-02-24 11:08:06",
        "updated_at": "2016-02-24 11:08:06",
        "slug": "Wrap",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Wrap",
        "description": "Wrap",
        "images": []
    }, {
        "id": "1191954629059937029",
        "order": null,
        "created_at": "2016-02-24 11:08:05",
        "updated_at": "2016-02-24 11:08:05",
        "slug": "Korridor-Design",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Korridor Design",
        "description": "Korridor Design",
        "images": []
    }, {
        "id": "1191954624874021636",
        "order": null,
        "created_at": "2016-02-24 11:08:04",
        "updated_at": "2016-02-24 11:08:04",
        "slug": "Polite.",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Polite.",
        "description": "Polite.",
        "images": []
    }, {
        "id": "1191954619564032771",
        "order": null,
        "created_at": "2016-02-24 11:08:04",
        "updated_at": "2016-02-24 11:08:04",
        "slug": "The-School-of-Life",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "The School of Life",
        "description": "The School of Life",
        "images": []
    }, {
        "id": "1191954614891578114",
        "order": null,
        "created_at": "2016-02-24 11:08:03",
        "updated_at": "2016-02-24 11:08:03",
        "slug": "Magpie",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Magpie",
        "description": "Magpie",
        "images": []
    }, {
        "id": "1191954609891967745",
        "order": null,
        "created_at": "2016-02-24 11:08:03",
        "updated_at": "2016-02-24 11:08:03",
        "slug": "Wild-&-Wolf",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Wild & Wolf",
        "description": "Wild & Wolf",
        "images": []
    }, {
        "id": "1191954603709563648",
        "order": null,
        "created_at": "2016-02-24 11:08:02",
        "updated_at": "2016-02-24 11:08:02",
        "slug": "Hold",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Hold",
        "description": "Hold",
        "images": []
    }, {
        "id": "1191954597636211455",
        "order": null,
        "created_at": "2016-02-24 11:08:01",
        "updated_at": "2016-02-24 11:08:01",
        "slug": "Serax",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Serax",
        "description": "Serax",
        "images": []
    }, {
        "id": "1191954592032621310",
        "order": null,
        "created_at": "2016-02-24 11:08:01",
        "updated_at": "2016-02-24 11:08:01",
        "slug": "Jay",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Jay",
        "description": "Jay",
        "images": []
    }, {
        "id": "1191954585715999485",
        "order": null,
        "created_at": "2016-02-24 11:08:00",
        "updated_at": "2016-02-24 11:08:00",
        "slug": "Floor_Story",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Floor_Story",
        "description": "Floor_Story",
        "images": []
    }, {
        "id": "1191954580573782780",
        "order": null,
        "created_at": "2016-02-24 11:07:59",
        "updated_at": "2016-02-24 11:07:59",
        "slug": "Penco",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Penco",
        "description": "Penco",
        "images": []
    }, {
        "id": "1191954576236872443",
        "order": null,
        "created_at": "2016-02-24 11:07:59",
        "updated_at": "2016-02-24 11:07:59",
        "slug": "FIELD",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "FIELD",
        "description": "FIELD",
        "images": []
    }, {
        "id": "1191954567512720122",
        "order": null,
        "created_at": "2016-02-24 11:07:58",
        "updated_at": "2016-02-24 11:07:58",
        "slug": "Nähe",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Nähe",
        "description": "Nähe",
        "images": []
    }, {
        "id": "1191954562873819897",
        "order": null,
        "created_at": "2016-02-24 11:07:57",
        "updated_at": "2016-02-24 11:07:57",
        "slug": "Midori",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Midori",
        "description": "Midori",
        "images": []
    }, {
        "id": "1191954556230042360",
        "order": null,
        "created_at": "2016-02-24 11:07:56",
        "updated_at": "2016-02-24 11:07:56",
        "slug": "Caran-d'Ache",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Caran d'Ache",
        "description": "Caran d'Ache",
        "images": []
    }, {
        "id": "1191953440855556855",
        "order": null,
        "created_at": "2016-02-24 11:05:43",
        "updated_at": "2016-02-24 11:05:43",
        "slug": "Demelza-Hill",
        "status": {
            "value": "Live",
            "data": {
                "key": "1",
                "value": "Live"
            }
        },
        "title": "Demelza Hill",
        "description": "Demelza Hill",
        "images": []
    }];

    this.groupedProducts = {
        "hay-kaleido-tray": {
            title: "HAY - KALEIDO TRAY",
            slug: "hay-kaleido-tray",
            price: "£9.00 - £50.00",
            image: "https://www.data.edited.co.uk/images/product/h/hay-kaleido-tray-304px-304px.png",
            allImages: ["https://www.data.edited.co.uk/images/product/h/hay-kaleido-tray-600px-600px.png"],
            description: "Kaleido is a beautifuly functional range of modular trays, forged from powder coated steel they create stunning collections of colour and shape when nested inside each other or laid out side to side. Perfect for serving coffee and drinks, organise jewellery or tidy your desk.",
            brand: "HAY",
            in_stock: 1,
            in_stock_text: "IN STOCK",
            matchLength: 3,
            isGroupedProduct: true,
            variants: ["Size", "Color"],
            variantArray: [{
                comboName: "Small",
                slug: "small",
                subVariants: [{
                    comboName: "Apricot"
                }]
            }, {
                comboName: "Medium",
                slug: "medium",
                subVariants: [{
                    comboName: "Chocolate"
                }]
            }, {
                comboName: "Large",
                slug: "large",
                subVariants: [{
                    comboName: "Orange"
                }, {
                    comboName: "Blue"
                }]
            }, {
                comboName: "X-Large",
                slug: "xlarge",
                subVariants: [{
                    comboName: "Aubergine"
                }, {
                    comboName: "Grey"
                }, {
                    comboName:"Jade"
                }]
            }]
        },
        "hay-bits-and-bobs-clear": {
            title: "HAY - Bits & Bobs",
            slug: "hay-bits-and-bobs-clear",
            price: "£8.00 - £10.00",
            image: "https://www.data.edited.co.uk/images/product/h/hay-bits-and-bobs-304px-304px.png",
            allImages: ["https://www.data.edited.co.uk/images/product/h/hay-bits-and-bobs-600px-600px.png"],
            description: "",
            brand: "HAY",
            in_stock: 1,
            in_stock_text: "IN STOCK",
            matchLength: 4,
            isGroupedProduct: true,
            variants: ["Size"],
            variantArray: [{
                comboName: "X-Small",
                slug: "xsmall"
            }, {
                comboName: "Small"
            }, {
                comboName: "Medium"
            }]
        },
        "hay-flowerpot-with-saucer": {
            title: "HAY - Flowerpot with Saucer",
            slug: "hay-flowerpot-with-saucer",
            price: "£9.00 - £15.00",
            image: "https://www.data.edited.co.uk/images/product/h/hay-flowerpot-with-saucer-(large-grey)-add-1-600px-600px.png",
            allImages: ["https://www.data.edited.co.uk/images/product/h/hay-flowerpot-with-saucer-(large-grey)-add-1-600px-600px.png"],
            description: "Flowerpot is a simple minimalist plant pot and saucer, made from polystone - a modern cross between stone and plastic.<br><br>Available in Black or Grey.<br><br>Medium measures 14cm x 13.5cm H.<br>Large measures 17.5cm x 16.5cm H.",
            brand: "HAY",
            in_stock: 1,
            in_stock_text: "IN STOCK",
            matchLength: 4,
            variants: ["Size", "Color"],
            isGroupedProduct: true,
            variantArray: [{
                comboName: "Large",
                slug: "large",
                subVariants: [{
                    comboName: "Grey",
                    slug: "grey"
                }, {
                    comboName: "Black",
                    slug: "black"
                }]
            }, {
                comboName: "Medium",
                slug: "medium",
                subVariants: [{
                    comboName: "Grey",
                    slug: "grey"
                }, {
                    comboName: "Black",
                    slug: "black"
                }]
            }]
        },
        "wrong-for-hay-sinker-pendant": {
            title: "Wrong for HAY - Sinker Pendant",
            slug: "wrong-for-hay-sinker-pendant",
            price: "£119.00 - £179.00",
            image: "https://www.data.edited.co.uk/images/product/w/wrong-for-hay-sinker-pendant-(large-dusty-grey)-add-1-600px-600px.png",
            allImages: ["https://www.data.edited.co.uk/images/product/w/wrong-for-hay-sinker-pendant-(large-dusty-grey)-add-1-600px-600px.png"],
            description: "Sinker's simple form and etched acrylic diffuser emits a warm, even source of illumination. The moulded ABS shade is supplied in a matt black or grey finish, and comes with a dimmable LED bulb.<br><br>The pendant is available in large and small size along with a ceiling light verison that mounts directly to a surface.<br><br>Each Sinker light is supplied with an LED bulb, the small size and ceiling verison comes with 1x E27 LED 10.5w Warm White dimmable lightbulb and the large size comes with 1x E27 LED 13w Warm White dimmable lightbulb.<br><br>Small Sinker Pendant measures 23cm W x 23cm D x 19cm H and is fitted with a 1.5m cord length in matching fabric covered cable.<br><br>Large Sinker Pendant measures 37.5cm W x 37.5cm D x 35cm H and is fitted with a 3m cord length in matching fabric covered cable.<br><br>Sinker Ceiling measures 23cm W x 23cm D x 19cm H.",
            brand: "Wrong for HAY",
            in_stock: 1,
            in_stock_text: "IN STOCK",
            matchLength: 4,
            variants: ["Size", "Color"],
            isGroupedProduct: true,
            variantArray: [{
                comboName: "Small",
                slug: "small",
                subVariants: [{
                    comboName: "Signal Black",
                    slug: "signal-black"
                }, {
                    comboName: "Dusty Grey",
                    slug: "dusty-grey"
                }]
            }, {
                comboName: "Large",
                slug: "large",
                subVariants: [{
                    comboName: "Signal Black",
                    slug: "signal-black"
                }, {
                    comboName: "Dusty Grey",
                    slug: "dusty-grey"
                }]
            }, {
                comboName: "Ceiling",
                slug: "ceiling",
                subVariants: [{
                    comboName: "Signal Black",
                    slug: "signal-black"
                }]
            }]
        },
        "hand-painted-plant-pot": {
            title: "Hand Painted Plant Pot",
            slug: "hand-painted-plant-pot",
            price: "£9.50",
            image: "https://www.data.edited.co.uk/images/product/h/hand-painted-plant-pot-(large-deep-blue)-r3761-304px-304px.png",
            allImages: ["https://www.data.edited.co.uk/images/product/h/hand-painted-plant-pot-(large-deep-blue)-r3761-600px-600px.png"],
            description: "These bold and versatile plant pots will give any house plant and home interior a new lease of life. Hand-painted in bright colours with a textured finish and made with strong terracota. <br><br>Available in Bright Orange, Coral, Fresh Mint, Cool Grey, Deep Blue and Teal.",
            brand: "Serax",
            in_stock: 1,
            in_stock_text: "IN STOCK",
            matchLength: 4,
            isGroupedProduct: true,
            variants: ["Colour"],
            variantArray: [{
                comboName: "Cool Grey",
                slug: "large-cool-grey"
            }, {
                comboName: "Fresh Mint",
                slug: "large-fresh-mint"
            }, {
                comboName: "Bright Orange",
                slug: "large-bright-orange"
            }, {
                comboName: "Petrol",
                slug: "large-petrol"
            }, {
                comboName: "Teal",
                slug: "large-teal"
            }, {
                comboName: "Coral",
                slug: "large-coral"
            }, {
                comboName: "Deep Blue",
                slug: "large-deep-blue"
            }]
        },
        "hay-revolver-stool": {
            title: "Hay - Revolver Stool",
            slug: "hay-revolver-stool",
            price: "£139.00 - £159.00",
            image: "images/hay-revolver-stool-all.png",
            allImages: ["images/hay-revolver-stool-all.png"],
            description: "A formal looking barstool made in square steel tube that is mounted onto a bearing mechanism to allow the seat and foot ring to turn 360 degrees. It has a slim, slightly dished seat with a curve on the edge for comfort. The base is weighty enough to create total stability. The rotating feature, while altering appearance also lends a sense of the play to this functional design. Finished in a smooth powder coat.<br><br>Available in Grey, Red or Signal Black and in two heights 65cm or 76cm.<br><br>Dimensions - 34cm Ø x 65cm",
            brand: "HAY",
            in_stock: 1,
            in_stock_text: "IN STOCK",
            matchLength: 3,
            variants: ["Size", "Color"],
            isGroupedProduct: true,
            variantArray: [{
                comboName: "65cm Medium",
                slug: "65cm",
                subVariants: [{
                    comboName: "Grey",
                    slug: "grey"
                }, {
                    comboName: "Red",
                    slug: "red"
                }, {
                    comboName: "Black",
                    slug: "black"
                }]
            }, {
                comboName: "76cm Large",
                slug: "76cm",
                subVariants: [{
                    comboName: "Grey",
                    slug: "grey"
                }, {
                    comboName: "Red",
                    slug: "red"
                }, {
                    comboName: "Black",
                    slug: "black"
                }]
            }]
        },
        "hay-j110-chair": {
            title: "Hay - J110 Chair",
            slug: "hay-j110-chair",
            price: "£179.00 - £189.00",
            image: "images/hay-j110-chair-all.jpeg",
            allImages: ["images/hay-j110-chair-all.jpeg"],
            description: "Tall slender wooden rods make up the backrest of this Nordic and democratic version of a royal seat. The rods form a cosy shell and offer a calm space to relax. J110 is an elegant dining chair with a built-in interplay of shadow and light when the sunlight darts in and out between the rods. J110 is an updated version of a classic chair with a solid history.<br><br>The FDB furniture series is functional design at democratic prices, born of industrial development. FDB created a strong furniture concept in 1933, and under Børge Mogensen’s leadership as design director from 1942 the FDB furniture linked industrial production closely with architectural furniture design. In practical terms, the industrial approach eliminated the need for special components, as all the chairs had the same legs, just as the main emphasis was not on detailing, since the underlying ideology of making well-designed furniture accessible to the general population was the primary focus. FDB revolutionized the industry as a precursor of many of the design icons that Denmark is acknowledged for today around the world.<br><br>The FDB series also pioneered knock-down furniture: furniture that comes in a box and is assembled by the user. Kvist Møbler produced the FDB furniture then and still does today – only now with HAY as the standard-bearer for the democratic values.",
            brand: "HAY",
            in_stock: 1,
            in_stock_text: "IN STOCK",
            matchLength: 3,
            isGroupedProduct: true,
            variants: ["Colour"],
            variantArray: [{
                comboName: "Nature",
                slug: "nature"
            }, {
                comboName: "Black",
                slug: "black"
            }, {
                comboName: "White",
                slug: "white"
            }, {
                comboName: "Grey",
                slug: "grey"
            }, {
                comboName: "Coral",
                slug: "coral"
            }]
        }
    }

});
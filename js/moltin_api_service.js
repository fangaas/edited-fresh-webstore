app.service('moltinApiService', function() {

	console.log("moltin service init");

   	/*$.ajax({
		url: "https://api.molt.in/oauth/access_token",
		type: "GET",
		beforeSend: function(xhr){xhr.setRequestHeader('X-Test-Header', 'test-value');},
		success: function() { alert('Success!' + authHeader); }
	});*/

    var moltin = new Moltin({publicId: '3fTqEGayHJKh2Gu0fNCypkE2DABja3A1WRa610o8'});
    moltin.Authenticate();

    var retryCount = 0,
    retryMax = 3;

    function handleApiError(error) {
    	//alert("Moltin Error: " + error);
    	console.log(error);
    }

    this.serviceFunction = function(funcData) {

        /*func data*/
        /*var funcData = {
            functionName: "name",
            callback: callback,
            extraField: extraField
        }*/

        try {
            switch(funcData.functionName) {
                case "getFilteredProducts":
                    moltin.Product.Find(funcData.filterObject, function(products, details) {
                        resetRetries();
                        funcData.callback(products, details);
                    }, handleApiError);

                break;
            }
        } catch(ex) {
            if(retryCount < 3) {
                retryCount++;
                moltin.Authenticate();
                this.serviceFunction(funcData);
            } else {
                handleApiError(ex);
            }
        }

        function resetRetries() {
            retryCount = 0;
        }

    }

    this.getSampleProduct = function() {
    	return moltin.Product.Find({slug: 'dog-hat'})[0];
    }

    this.getAllProducts = function(callback) {
    	moltin.Product.List(null, function(product) {
		    callback(product);
		}, handleApiError);
    }

    this.getFilteredProducts = function(filterObject, callback) {

        /*moltin.Product.Find(filterObject, function(products, details) {
            callback(products, details);
        }, handleApiError);*/

        this.serviceFunction({
            functionName: "getFilteredProducts",
            filterObject: filterObject,
            callback: callback
        });

    }

    this.getSearchProducts = function(filterObject, callback) {
        moltin.Product.Search(filterObject, function(products, details) {
            callback(products, details);
        }, handleApiError);
    }

    this.getCategoryTree = function(callback) {
    	moltin.Category.Tree(null, function(tree) {
    		callback(tree)
    	}, handleApiError);
    }

    this.getBrands = function(callback) {
        moltin.Brand.List(null, function(brands) {
            callback(brands);
            //console.log(JSON.stringify(brands));
        }, handleApiError);
    }

    /*Product handlers*/
    this.addProductToCart = function(productId, quantity, callback) {

        moltin.Cart.Insert(productId, quantity, null, function(cart) {
            callback(cart); 
        }, handleApiError);

    }

    this.getCartData = function(customerId, callback) {

        if(customerId == null) {
            moltin.Cart.Contents(function(cart) {
                callback(cart);
            }, handleApiError);            
        } else {
            moltin.Cart.Contents(customerId, function(cart) {
                callback(cart);
            }, handleApiError);
        }

    }

    this.removeFromCart = function(itemId, callback) {

        moltin.Cart.Remove(itemId, callback, handleApiError);

    }

    this.clearCart = function(callback, errorCallback) {

        if(errorCallback == undefined) errorCallback = handleApiError;

        moltin.Cart.Delete(callback, errorCallback);

    }

    this.clearCartAndGotoReceipt = function(orderId, callback, errorCallback) {

        moltin.Cart.Delete(callback(orderId), errorCallback(orderId));  

    }

    this.updateCartQuantity = function(itemId, quantity, callback) {

        moltin.Cart.Update(itemId, {
            quantity: quantity
        }, callback, handleApiError);

    }

    this.applyPromoCode = function(code, callback, errorCallback) {

        moltin.Cart.Discount(code, callback, errorCallback);

    }

    this.getAllShippingMethods = function(callback) {

        moltin.Shipping.List(null, callback);

    }

    this.getAllGateways = function(callback) {

        moltin.Gateway.List(null, callback);

    }

    //PAYMENT PROCESSING
    this.createOrderFromCart = function(orderJson, callback, errorCallback) {

        moltin.Cart.Complete(orderJson, callback, errorCallback);

    }

    this.payForOrderOnsite = function(orderId, cardData, callback, errorCallback) {

        moltin.Checkout.Payment('purchase', orderId, cardData, callback, errorCallback);

    }


});
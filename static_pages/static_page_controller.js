app.controller('static_page_controller', ['$scope', '$location', 'eventTracking', function($scope, $location, eventTracking) {

	$(function() {

		$("body").animate({scrollTop:0});
		$('.sidebar-menu ul').slideUp(300);

	});

	//$scope.viewCategory = null;
	//$scope.cat = "";

	$scope.setCatForSidebar("");


	var pagePath = $location.path().replace("/","");

	eventTracking.goToStaticPage(pagePath)

}])